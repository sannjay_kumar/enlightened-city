package com.ty.enlightened.enlightenedcity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.Hotel;

public interface HotelRepository extends JpaRepository<Hotel, String> {

	public List<Hotel> findByArea(String area);
}
