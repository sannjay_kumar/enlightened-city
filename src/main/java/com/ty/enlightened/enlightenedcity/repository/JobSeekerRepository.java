package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.JobSeeker;

public interface JobSeekerRepository extends JpaRepository<JobSeeker, String>{

}
