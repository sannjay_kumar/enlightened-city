package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.JobByDesignation;



public interface JobByDesignationRepository extends JpaRepository<JobByDesignation, String> {

}
