package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.SkillDevelopmentInstitute;

public interface SkillDevelopmentInstituteRepository extends JpaRepository<SkillDevelopmentInstitute, String>{

}
