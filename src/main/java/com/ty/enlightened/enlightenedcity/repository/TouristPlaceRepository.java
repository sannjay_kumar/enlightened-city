package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.TouristPlaces;

public interface TouristPlaceRepository extends JpaRepository<TouristPlaces,String >{
	
}
