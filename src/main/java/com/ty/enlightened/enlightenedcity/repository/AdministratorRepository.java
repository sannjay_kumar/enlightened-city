package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.Administrator;

public interface AdministratorRepository extends JpaRepository<Administrator, String> {

}
