package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.Booking;

public interface BookingRepository extends JpaRepository<Booking, String> {

}
