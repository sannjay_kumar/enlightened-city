package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.Room;

public interface RoomRepository extends JpaRepository<Room, String>{
	
}
