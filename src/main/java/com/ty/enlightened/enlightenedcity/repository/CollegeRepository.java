package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.College;

public interface CollegeRepository extends JpaRepository<College, String> {
	public College findByName(String name);
}
