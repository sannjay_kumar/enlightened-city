package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.SportsClub;

public interface SportClubRepository extends JpaRepository<SportsClub, String> {
}
