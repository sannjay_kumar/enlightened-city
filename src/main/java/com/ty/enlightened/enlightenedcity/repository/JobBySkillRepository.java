package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.JobBySkill;

public interface JobBySkillRepository extends JpaRepository<JobBySkill, String> {

}
