package com.ty.enlightened.enlightenedcity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.enlightened.enlightenedcity.dto.Company;

public interface CompanyRepository  extends JpaRepository<Company,String>{

}
