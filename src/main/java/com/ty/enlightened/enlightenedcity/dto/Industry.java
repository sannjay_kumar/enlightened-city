package com.ty.enlightened.enlightenedcity.dto;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class Industry {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emp_seq")
	@GenericGenerator(name = "emp_seq", strategy = "com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator", parameters = {
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.INCREMENT_PARAM, value = "50"),
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.VALUE_PREFIX_PARAMETER, value = "Adm_"),
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private String I_id;
	private String name;
	private int pincode;
	private String type;

	@OneToMany(mappedBy = "industry")
	private List<Company> company;

	public String getI_id() {
		return I_id;
	}

	public void setI_id(String i_id) {
		I_id = i_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Industry [I_id=" + I_id + ", name=" + name + ", pincode=" + pincode + ", type=" + type + "]";
	}

//	@OneToMany(mappedBy ="industry")
//	private Company company;
}
