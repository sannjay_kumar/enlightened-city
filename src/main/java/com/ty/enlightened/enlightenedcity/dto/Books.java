package com.ty.enlightened.enlightenedcity.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.Data;

@Entity
@Data
public class Books {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stu_seq")
	@GenericGenerator(name = "stu_seq", strategy = "com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator", parameters = {
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.INCREMENT_PARAM, value = "50"),
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.VALUE_PREFIX_PARAMETER, value = "BOOK_"),
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private int id;
	private String name;
	private String author;
	private String date;
	@ManyToOne
	@JoinColumn(name = "library_id")
	private Library library;
}
