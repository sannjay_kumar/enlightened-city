package com.ty.enlightened.enlightenedcity.dto;

public class StoredOTP {
	private static int otp;

	public static int getOtp() {
		return otp;
	}

	public static void setOtp(int otp) {
		StoredOTP.otp = otp;
	}
}
