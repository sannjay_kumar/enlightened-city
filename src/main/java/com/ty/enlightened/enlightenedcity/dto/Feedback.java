package com.ty.enlightened.enlightenedcity.dto;

import javax.validation.constraints.Email;

public class Feedback {
	
	private String subject;
	private String to;
	@Email
	private String body;
	
	public Feedback(String subject, @Email String to, String body) {
		this.subject = subject;
		this.to = to;
		this.body = body;
	}
	
	public String getName() {
		return subject;
	}
	
	public String getFeedback() {
		return body;
	}
	public void setName(String name) {
		this.subject = name;
	}
	
	public void setFeedback(String feedback) {
		this.body = feedback;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "Feedback [name=" + subject + ", to=" + to + ", feedback=" + body + "]";
	}

}
