package com.ty.enlightened.enlightenedcity.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class TouristPlaces {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "building_seq")
	@GenericGenerator(name = "building_seq", strategy = "com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator", parameters = {
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.INCREMENT_PARAM, value = "50"),
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.VALUE_PREFIX_PARAMETER, value = "SATYA_TOURIST_"),
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private String tId;
	private String placeName;
	private String placeAddress;
	private long pin;
	private double entryFees;
	private String timing;
	public String gettId() {
		return tId;
	}
	public void settId(String tId) {
		this.tId = tId;
	}
	public String getPlaceName() {
		return placeName;
	}
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}
	public String getPlaceAddress() {
		return placeAddress;
	}
	public void setPlaceAddress(String placeAddress) {
		this.placeAddress = placeAddress;
	}
	public long getPin() {
		return pin;
	}
	public void setPin(long pin) {
		this.pin = pin;
	}
	public double getEntryFees() {
		return entryFees;
	}
	public void setEntryFees(double entryFees) {
		this.entryFees = entryFees;
	}
	public String getTiming() {
		return timing;
	}
	public void setTiming(String timing) {
		this.timing = timing;
	}
	
	}
