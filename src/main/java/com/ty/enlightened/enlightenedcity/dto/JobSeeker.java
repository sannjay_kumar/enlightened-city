 package com.ty.enlightened.enlightenedcity.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class JobSeeker {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emp_seq")
	@GenericGenerator(name = "emp_seq", strategy = "com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator", parameters = {
	@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.INCREMENT_PARAM, value = "50"),
	@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.VALUE_PREFIX_PARAMETER, value = "Adm_"),
	@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private String sId;
	private String sName;
	private int sAge;
	private long sPhno;
	private String sGender;
	private String sQualification;
	
	@ManyToOne
	@JoinColumn
	private Company company;
	
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public int getsAge() {
		return sAge;
	}
	public void setsAge(int sAge) {
		this.sAge = sAge;
	}
	public long getsPhno() {
		return sPhno;
	}
	public void setsPhno(long sPhno) {
		this.sPhno = sPhno;
	}
	public String getsGender() {
		return sGender;
	}
	public void setsGender(String sGender) {
		this.sGender = sGender;
	}
	public String getsQualification() {
		return sQualification;
	}
	public void setsQualification(String sQualification) {
		this.sQualification = sQualification;
	}
	@Override
	public String toString() {
		return "JobSeeker [sId=" + sId + ", sName=" + sName + ", sAge=" + sAge + ", sPhno=" + sPhno + ", sGender="
				+ sGender + ", sQualification=" + sQualification + ", company=" + company + "]";
	}
	
	
	
	
	
}
