package com.ty.enlightened.enlightenedcity.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
@Entity
public class ShoppingMalls {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "building_seq")
	@GenericGenerator(name = "building_seq", strategy = "com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator", parameters = {
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.INCREMENT_PARAM, value = "50"),
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.VALUE_PREFIX_PARAMETER, value = "SATYA_SHOPPINGMALL_"),
			@Parameter(name = com.ty.enlightened.enlightenedcity.idgenerator.CustomIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private String mid;
	private String mName;
	private int mPin;
	private String mArea;
	private String mAbout;
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public int getmPin() {
		return mPin;
	}
	public void setmPin(int mPin) {
		this.mPin = mPin;
	}
	public String getmArea() {
		return mArea;
	}
	public void setmArea(String mArea) {
		this.mArea = mArea;
	}
	public String getmAbout() {
		return mAbout;
	}
	public void setmAbout(String mAbout) {
		this.mAbout = mAbout;
	}
	
}
