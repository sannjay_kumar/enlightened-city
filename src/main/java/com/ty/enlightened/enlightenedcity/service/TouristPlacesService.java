package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.TouristPlaceDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.TouristPlaces;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class TouristPlacesService {
	@Autowired
	private TouristPlaceDao touristPlaceDao;
	
	public ResponseStructure<TouristPlaces> saveTouristPlaces(TouristPlaces places) {
		
		touristPlaceDao.save(places);
		ResponseStructure<TouristPlaces> response = new ResponseStructure<TouristPlaces>();
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Success");
		response.setData(places);
		return response;
	}
	
	public ResponseStructure<TouristPlaces> getTouristById(String tId) {
		
		TouristPlaces touristPlaces=touristPlaceDao.touristPlacesById(tId);
		if(touristPlaces==null) {
			 throw new NoIdFoundException();
		}
		ResponseStructure<TouristPlaces> response = new ResponseStructure<TouristPlaces>();
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Success");
		response.setData(touristPlaces);
		return response;
	}
	
	public ResponseStructure<List<TouristPlaces>> allTourist(){
		
		List<TouristPlaces> ls=touristPlaceDao.getAllTouristPlaces();
		if(ls!=null) {
			ResponseStructure<List<TouristPlaces>> response = new ResponseStructure<List<TouristPlaces>>();
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			response.setData(ls);
			return response;
		}
		 throw new NoIdFoundException();
	}
	
	public ResponseStructure<TouristPlaces>updatetouristById(String id,TouristPlaces touristPlaces){
		TouristPlaces tPlaces=touristPlaceDao.updateTPlacesById(id, touristPlaces);
		if(tPlaces!=null) {
			ResponseStructure<TouristPlaces> responseStructure=new ResponseStructure<TouristPlaces>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage(id);
			responseStructure.setData(tPlaces);
			return responseStructure;
		}
		return null;
	}
	
	public ResponseStructure<TouristPlaces> deleteTouristPlacesById(String id) {
		TouristPlaces tr=touristPlaceDao.touristPlacesById(id);
		if(tr!=null) {
		touristPlaceDao.deleteById(id);
		ResponseStructure<TouristPlaces> response = new ResponseStructure<TouristPlaces>();
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Success");
		return response;
		}
		 throw new NoIdFoundException();
	}
	
	
	
}
