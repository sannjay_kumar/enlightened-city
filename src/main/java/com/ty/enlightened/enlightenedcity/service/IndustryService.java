package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.IndustryDao;
import com.ty.enlightened.enlightenedcity.dto.Industry;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;


@Service
public class IndustryService {

	@Autowired
	
	private IndustryDao dao;
	
	public ResponseStructure<Industry> saveIndustry(Industry industry)
	{
		Industry industry1=dao.saveIndustry(industry);
		ResponseStructure<Industry> rs=new ResponseStructure<Industry>();
		rs.setStatus(HttpStatus.OK.value());
		rs.setMessage("success");
	rs.setData(industry1);
	return rs;
		}
		
	public ResponseStructure<Industry> getIndustryById(String I_id) {
		Industry industry = dao.getIndustryById(I_id);
		ResponseStructure<Industry> rs=new ResponseStructure<Industry>();
		if(industry == null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + I_id + " does not exist");
		}
		
	}
	  

	public ResponseStructure<Industry> updateIndustryById(Industry industry, String I_id) {
		Industry industry1=dao.updateIndustryById(industry, I_id);
		ResponseStructure<Industry> rs=new ResponseStructure<Industry>();
		if (industry1 != null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("updated");
			
		
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + I_id + " does not exist");	
		}

	}
	public ResponseStructure<Industry> deleteIndustryById(String I_id) {
		boolean res=dao.deleteIndustryById(I_id);
		ResponseStructure<Industry> rs=new ResponseStructure<Industry>();
		if(res==true)
		{
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("deleted");
		    

		
		return rs;
		
		}
		else {
			throw new NoIdFoundException("given " + I_id + " does not exist");	
		}
		}
}
