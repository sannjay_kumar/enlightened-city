 package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.BookingDao;
import com.ty.enlightened.enlightenedcity.dto.Booking;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Room;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class BookingService {
	@Autowired
	private BookingDao bdao;
	public ResponseStructure<Booking> saveBooking(String hotelid, Booking booking) {
		double cost=0;
		List<Room> ls=booking.getRoom();
		for(Room room:ls) {
			cost=cost+room.getRent();
		}
		booking.setCost(cost);
		bdao.saveBooking(hotelid, booking);
		ResponseStructure<Booking> response = new ResponseStructure<Booking>();
		if (hotelid != null) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			response.setData(booking);
			return response;
		}
		 throw new NoIdFoundException();
	}

	public ResponseStructure<Booking> getBookingId(String bookid) {
		Booking booking = bdao.getBookingById(bookid);
		ResponseStructure<Booking> response = new ResponseStructure<Booking>();
		if (bookid != null) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			response.setData(booking);
			return response;

		}
		 throw new NoIdFoundException();
	}

	public ResponseStructure<List<Booking>> getAllBooking(String hotelid) {
		List<Booking> li = bdao.getAllBooking(hotelid);
		ResponseStructure<List<Booking>> response = new ResponseStructure<List<Booking>>();
		if (li != null) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			response.setData(li);
			return response;
		}
		 throw new NoIdFoundException();
	}

	public ResponseStructure<Booking> deleteBooking(String bookid) {
		if (bookid != null) {
			bdao.deleteBooking(bookid);
			ResponseStructure<Booking> response = new ResponseStructure<Booking>();
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			return response;
		} else
			 throw new NoIdFoundException();
	}

	public ResponseStructure<Booking> updateBooking(String bookid, Booking booking) {
		if (bdao.updateBooking(bookid, booking) != null) {
			ResponseStructure<Booking> response = new ResponseStructure<Booking>();
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			response.setData(booking);
			return response;
		}
		 throw new NoIdFoundException();
	}

}
