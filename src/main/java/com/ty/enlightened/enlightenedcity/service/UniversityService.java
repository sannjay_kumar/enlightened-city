package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dao.UniversityDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.University;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Repository
public class UniversityService {
	@Autowired
	private UniversityDao dao;
	
	public ResponseStructure<University> saveUniversity(University university) {
		University university2 = dao.saveUniversity(university);
		ResponseStructure<University> structure = new ResponseStructure<University>();
		if (university2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(university2);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<University> getUniversityById(String id) {
		University university = dao.getUniversityById(id);
		ResponseStructure<University> structure = new ResponseStructure<University>();
		if (university != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(university);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<List<University>> getAllUniversitys(){
		List<University> universities = dao.getAllUniversities();
		ResponseStructure<List<University>> structure = new ResponseStructure<List<University>>();
		if (universities != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(universities);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("NO DATA FOUND");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Boolean> deleteUniversityById(String id) {
		Boolean status = dao.deleteUniversityById(id);
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<University> updateUniversityById(String id, University university) {
		University university2 = dao.updateUniversityById(id, university);
		ResponseStructure<University> structure = new ResponseStructure<University>();
		if (university2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(university2);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
}
