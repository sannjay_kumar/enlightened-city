package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.RoomDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Room;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class RoomService {
	@Autowired
	private RoomDao roomDao;

		public ResponseStructure<Room> saveRoom(String bid, Room room) {
			roomDao.saveRoom(bid,room);
			ResponseStructure<Room> response = new ResponseStructure<Room>();
			if (bid != null) {
				response.setStatus(HttpStatus.OK.value());
				response.setMessage("Success");
				response.setData(room);
				return response;
			}
		   throw new NoIdFoundException();

		}

		public ResponseStructure<Room> getRoomId(String rid) {
			Room room = roomDao.getRoomById(rid);
			ResponseStructure<Room> response = new ResponseStructure<Room>();
			if (room!= null) {
				response.setStatus(HttpStatus.OK.value());
				response.setMessage("Success");
				response.setData(room);
				return response;

			}
			throw new NoIdFoundException();
		}

		public ResponseStructure<List<Room>> getAllRoom() {
			List<Room> li = roomDao.allRoom();
			ResponseStructure<List<Room>> response = new ResponseStructure<List<Room>>();
			if (li != null) {
				response.setStatus(HttpStatus.OK.value());
				response.setMessage("Success");
				response.setData(li);
				return response;
			}
			throw new NoIdFoundException();
		}

		public ResponseStructure<Boolean> deleteRoom(String rid) {
			if (roomDao.deleteRoomById(rid)==true) {
				ResponseStructure<Boolean> response = new ResponseStructure<Boolean>();
				response.setStatus(HttpStatus.OK.value());
				response.setMessage("Success");
				return response;
			} 
			throw new NoIdFoundException();
		}

		public ResponseStructure<Room> updateRoom(String rid, Room room) {
			if (roomDao.updateRoomById(rid, room) != null) {
				ResponseStructure<Room> response = new ResponseStructure<Room>();
				response.setStatus(HttpStatus.OK.value());
				response.setMessage("Success");
				response.setData(room);
				return response;
			}
			throw new NoIdFoundException();
		}

	


}
