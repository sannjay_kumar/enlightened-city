package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.CoachingCentreDao;
import com.ty.enlightened.enlightenedcity.dto.CoachingCentre;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;

@Service
public class CoachingCentreservice {
	@Autowired
	private CoachingCentreDao coachingCentreDao ;
	public ResponseStructure<CoachingCentre> saveCoachingCentre(CoachingCentre coachingCentre) {
		CoachingCentre centre = coachingCentreDao.saveCoachingCentr(coachingCentre);
		ResponseStructure<CoachingCentre> structure = new ResponseStructure<CoachingCentre>();
		if (centre != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(centre);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	public ResponseStructure<CoachingCentre> UpdatecoachingCentreById(CoachingCentre coachingCentre,String id) {
		CoachingCentre centre = coachingCentreDao.updateCoachingCentre(coachingCentre, id);
		ResponseStructure<CoachingCentre> structure = new ResponseStructure<CoachingCentre>();
		if (centre != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(centre);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	public ResponseStructure<CoachingCentre> getCoachingCentreById(String id) {
		CoachingCentre centre = coachingCentreDao.getCoachingCentreById(id);
		ResponseStructure<CoachingCentre> structure = new ResponseStructure<CoachingCentre>();
		if (centre != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(centre);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("ID DATA FOUND");
			structure.setData(null);
		}
		return structure;
	
		}
	public ResponseStructure<List<CoachingCentre>> getAllCoachingCentres(){
		List<CoachingCentre> centres = coachingCentreDao.getAllCoachingCentre();
		ResponseStructure<List<CoachingCentre>> structure = new ResponseStructure<List<CoachingCentre>>();
		if (centres != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(centres);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("NO DATA FOUND");
			structure.setData(null);
		}
		return structure;
	}
	public  ResponseStructure<Boolean> deleteCoachingCentreById(String id){
		Boolean status = coachingCentreDao.deleteCoachingCentreById(id);
		
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("ID DATA FOUND");
			structure.setData(status);
		}
		return structure;
	 
		
		
	}
	

}
