package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.CoachingCentreBranchDao;
import com.ty.enlightened.enlightenedcity.dto.CoachingCentreBranch;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
@Service
public class CoachingCentreBranchBranchservice {
	@Autowired
	private CoachingCentreBranchDao coachingCentreBranchDao ;
	public ResponseStructure<CoachingCentreBranch> saveCoachingCentreBranch(CoachingCentreBranch coachingCentreBranch) {
		CoachingCentreBranch centre = coachingCentreBranchDao.saveCoachingCentr(coachingCentreBranch);
		ResponseStructure<CoachingCentreBranch> structure = new ResponseStructure<CoachingCentreBranch>();
		if (centre != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(centre);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	public ResponseStructure<CoachingCentreBranch> UpdateCoachingCentreBranchById(CoachingCentreBranch coachingCentreBranch,String id) {
		CoachingCentreBranch centre = coachingCentreBranchDao.updateCoachingCentreBranch(coachingCentreBranch, id);
		ResponseStructure<CoachingCentreBranch> structure = new ResponseStructure<CoachingCentreBranch>();
		if (centre != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(centre);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	public ResponseStructure<CoachingCentreBranch> getCoachingCentreBranchById(String id) {
		CoachingCentreBranch centre = coachingCentreBranchDao.getCoachingCentreBranchById(id);
		ResponseStructure<CoachingCentreBranch> structure = new ResponseStructure<CoachingCentreBranch>();
		if (centre != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(centre);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("ID DATA FOUND");
			structure.setData(null);
		}
		return structure;
	
		}
//	public ResponseStructure<List<CoachingCentreBranch>> getAllCoachingCentreBranchs(){
//		List<CoachingCentreBranch> centres = coachingCentreBranchDao.getAllCoachingCentreBranch();
//		ResponseStructure<List<CoachingCentreBranch>> structure = new ResponseStructure<List<CoachingCentreBranch>>();
//		if (centres != null) {
//			structure.setStatus(HttpStatus.OK.value());
//			structure.setMessage("Success");
//			structure.setData(centres);
//		} else {
//			structure.setStatus(HttpStatus.NOT_FOUND.value());
//			structure.setMessage("NO DATA FOUND");
//			structure.setData(null);
//		}
//		return structure;
	
	public  ResponseStructure<Boolean> deleteCoachingCentreById(String id){
		Boolean status = coachingCentreBranchDao.deleteCoachingCentreBranchById(id);
		
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("ID DATA FOUND");
			structure.setData(status);
		}
		return structure;
		
	}
	

}
