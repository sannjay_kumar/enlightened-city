package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.JobByDesignationDao;
import com.ty.enlightened.enlightenedcity.dto.JobByDesignation;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class JobByDesignationService {

	@Autowired
	private JobByDesignationDao dao;

	public ResponseStructure<JobByDesignation> saveJobByDesignation(JobByDesignation jobByDesignation)
	{
		JobByDesignation jobByDesignation1=dao.saveJobByDesignation(jobByDesignation);
		ResponseStructure<JobByDesignation> rs=new ResponseStructure<JobByDesignation>();
		rs.setStatus(HttpStatus.OK.value());
		rs.setMessage("success");
		return rs;
		}

	public ResponseStructure<JobByDesignation> getJobByDesignation(String id) {
		JobByDesignation jobByDesignation1 = dao.getJobByDesignationById(id);
		ResponseStructure<JobByDesignation> rs=new ResponseStructure<JobByDesignation>();
		if(jobByDesignation1 == null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + id + " does not exist");
		}
		
	}

	public ResponseStructure<JobByDesignation> updateJobByDesignationById(JobByDesignation jobByDesignation, String id) {
		JobByDesignation jobByDesignation1 =dao.getJobByDesignationById(id);
		ResponseStructure<JobByDesignation> rs=new ResponseStructure<JobByDesignation>();
		if (jobByDesignation1 != null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + id + " does not exist");
		}

	}

	public boolean deleteJobByDesignationById(String id) {
		return dao.deleteJobByDesignationById(id);
	}
}
