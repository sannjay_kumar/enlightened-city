package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.JobSeekerDao;
import com.ty.enlightened.enlightenedcity.dto.JobSeeker;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class JobSeekerService {	
	

	@Autowired
	private JobSeekerDao dao;

	public ResponseStructure<JobSeeker> saveJobSeeker(JobSeeker jobSeeker)
	{
		JobSeeker jobSeeker1=dao.saveJobSeeker(jobSeeker);
		ResponseStructure<JobSeeker> rs=new ResponseStructure<JobSeeker>();
		rs.setStatus(HttpStatus.OK.value());
		rs.setMessage("success");
		return rs;
		}

	public ResponseStructure<JobSeeker> getJobSeeker(String sId) {
		JobSeeker jobSeeker1 = dao.getJobSeekerById(sId);
		ResponseStructure<JobSeeker> rs=new ResponseStructure<JobSeeker>();
		if(jobSeeker1 == null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + sId + " does not exist");
		}
		
	}

	public ResponseStructure<JobSeeker> updateJobSeekerById(JobSeeker jobSeeker, String sId) {
		JobSeeker jobSeeker1 =dao.getJobSeekerById(sId);
		ResponseStructure<JobSeeker> rs=new ResponseStructure<JobSeeker>();
		if (jobSeeker1 != null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + sId + " does not exist");
		}

	}
	public boolean deleteJobSeekerById(String sId) {
		return dao.deleteJobSeekerById(sId);
	}
	
}
