package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.JobBySkillDao;
import com.ty.enlightened.enlightenedcity.dto.JobBySkill;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class JobBySkillService {
	
	@Autowired
	private JobBySkillDao dao;

	public ResponseStructure<JobBySkill> saveJobBySkill(JobBySkill jobBySkill)
	{
		JobBySkill jobBySkill1=dao.saveJobBySkill(jobBySkill);
		ResponseStructure<JobBySkill> rs=new ResponseStructure<JobBySkill>();
		rs.setStatus(HttpStatus.OK.value());
		rs.setMessage("success");
		return rs;
		}

	public ResponseStructure<JobBySkill> getJobBySkill(String id) {
		JobBySkill jobBySkill1 = dao.getJobBySkillById(id);
		ResponseStructure<JobBySkill> rs=new ResponseStructure<JobBySkill>();
		if(jobBySkill1 == null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + id + " does not exist");
		}
		
	}

	public ResponseStructure<JobBySkill> updateJobBySkillById(JobBySkill jobBySkill, String id) {
		JobBySkill jobBySkill1 =dao.getJobBySkillById(id);
		ResponseStructure<JobBySkill> rs=new ResponseStructure<JobBySkill>();
		if (jobBySkill1 != null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + id + " does not exist");
		}

	}
	public boolean deleteJobBySkillById(String id) {
		return dao.deleteJobBySkillById(id);
	}
}
