package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.SportsClubDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.SportsClub;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class SportsClubService {
	@Autowired
	private SportsClubDao dao;
	
	public ResponseStructure<SportsClub> saveSportsClub(SportsClub sportsClub) {
		SportsClub sportsClub2 = dao.saveSportsClub(sportsClub);
		ResponseStructure<SportsClub> structure = new ResponseStructure<SportsClub>();
		if (sportsClub2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(sportsClub2);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<SportsClub> getSportsClubById(String id) {
		SportsClub sportsClub = dao.getSportsClubById(id);
		ResponseStructure<SportsClub> structure = new ResponseStructure<SportsClub>();
		if (sportsClub != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(sportsClub);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<List<SportsClub>> getAllSportsClubs(){
		List<SportsClub> sportsClubs = dao.getAllSportsClubs();
		ResponseStructure<List<SportsClub>> structure = new ResponseStructure<List<SportsClub>>();
		if (sportsClubs != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(sportsClubs);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("DATA NOT FOUND");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Boolean> deleteSportsClubById(String id) {
		boolean status = dao.deleteSportsClubById(id);
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<SportsClub> updateSportsClubById(String id, SportsClub sportsClub) {
		SportsClub sportsClub2 = dao.updateSportsClub(id, sportsClub);
		ResponseStructure<SportsClub> structure = new ResponseStructure<SportsClub>();
		if (sportsClub2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(sportsClub2);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
}
