package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.CollegeDao;
import com.ty.enlightened.enlightenedcity.dto.College;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class CollegeService {
	@Autowired
	private CollegeDao dao;
	
	public ResponseStructure<College> saveCollege(College college) {
		College college2 = dao.saveCollege(college);
		ResponseStructure<College> structure = new ResponseStructure<College>();
		if (college2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(college2);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<College> getCollegeByName(String name) {
		College college = dao.getCollegeByName(name);
		ResponseStructure<College> structure = new ResponseStructure<College>();
		if (college != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(college);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<College> getCollegeById(String id) {
		College college = dao.getCollegeById(id);
		ResponseStructure<College> structure = new ResponseStructure<College>();
		if (college != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(college);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<List<College>> getAllColleges(){
		List<College> colleges = dao.getAllColleges();
		ResponseStructure<List<College>> structure = new ResponseStructure<List<College>>();
		if (colleges != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(colleges);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("NO DATA FOUND");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Boolean> deleteCollegeById(String id) {
		Boolean status = dao.deleteCollegeById(id);
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<College> updateCollegeById(String id, College college) {
		College college2 = dao.updateCollegeById(id, college);
		ResponseStructure<College> structure = new ResponseStructure<College>();
		if (college2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(college2);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
}
