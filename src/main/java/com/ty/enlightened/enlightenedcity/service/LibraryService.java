package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.LibraryDao;
import com.ty.enlightened.enlightenedcity.dto.Books;
import com.ty.enlightened.enlightenedcity.dto.Library;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class LibraryService {
	@Autowired
	private LibraryDao dao;
	
	public ResponseStructure<Library> saveLibrary(Library library) {
		Library library2 = dao.saveLibrary(library);
		ResponseStructure<Library> structure = new ResponseStructure<Library>();
		if (library2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(library2);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Library> getLibraryById(String id) {
		Library library = dao.getLibraryById(id);
		ResponseStructure<Library> structure = new ResponseStructure<Library>();
		if (library != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(library);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<List<Library>> getAllLibrarys(){
		List<Library> libraries = dao.getAllLibraries();
		ResponseStructure<List<Library>> structure = new ResponseStructure<List<Library>>();
		if (libraries != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(libraries);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("NO DATA FOUND");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Boolean> deleteLibraryById(String id) {
		Boolean status = dao.deleteLibraryById(id);
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<Library> updateLibraryById(String id, Library library) {
		Library library2 = dao.updateLibraryById(id, library);
		ResponseStructure<Library> structure = new ResponseStructure<Library>();
		if (library2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(library2);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
}
