package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.SkillDevelopmentInstituteDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.SkillDevelopmentInstitute;
@Service
public class SkillDevelopmentInstituteservice {
	@Autowired
	private SkillDevelopmentInstituteDao skillDevelopmentInstituteDao ;
	public ResponseStructure<SkillDevelopmentInstitute>saveSkillDevelopmentInstitute(SkillDevelopmentInstitute developmentInstitute) {
		SkillDevelopmentInstitute skillDevelopmentInstitute = skillDevelopmentInstituteDao.saveSkillDevelopmentInstitute(developmentInstitute);
		ResponseStructure<SkillDevelopmentInstitute> structure =new ResponseStructure<SkillDevelopmentInstitute>();
		if(skillDevelopmentInstitute !=null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("sucess");
			structure.setData(skillDevelopmentInstitute);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("failed");
			structure.setData(null);
			
			
		}
		return  structure;
	}
	public ResponseStructure<SkillDevelopmentInstitute> UpdateSkillDevelopmentInstituteById(String id) {
		SkillDevelopmentInstitute skillDevelopmentInstitute = skillDevelopmentInstituteDao.getSkillDevelopmentInstituteById(id);
		ResponseStructure<SkillDevelopmentInstitute> structure = new ResponseStructure<SkillDevelopmentInstitute>();
		if(skillDevelopmentInstitute !=null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("sucess");
			structure.setData(skillDevelopmentInstitute);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("failed");
			structure.setData(null);
			
			
		}
		return  structure;
	}
	public ResponseStructure<SkillDevelopmentInstitute> getSkillDevelopmentInstituteById(String id) {
		SkillDevelopmentInstitute skillDevelopmentInstitute = skillDevelopmentInstituteDao.getSkillDevelopmentInstituteById(id);
		ResponseStructure<SkillDevelopmentInstitute> structure = new ResponseStructure<SkillDevelopmentInstitute>();
		if(skillDevelopmentInstitute !=null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("sucess");
			structure.setData(skillDevelopmentInstitute);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("failed");
			structure.setData(null);
			
			
		}
		return  structure;
		
	
		}
//	public List<SkillDevelopmentInstitute>getAllSkillDevelopmentInstitutes(){
//		return SkillDevelopmentInstituteDao.getAllSkillDevelopmentInstitute();
	
	public  ResponseStructure<Boolean> SkillDevelopmentInstituteserviceById(String id){
		Boolean status = skillDevelopmentInstituteDao.deleteSkillDevelopmentInstituteById(id);
		
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("ID DATA FOUND");
			structure.setData(status);
		}
		return structure;
		

}
}
