package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.BooksDao;
import com.ty.enlightened.enlightenedcity.dto.Books;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Student;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class BooksService {
	@Autowired
	private BooksDao dao;
	
	public ResponseStructure<Books> saveBooks(Books books) {
		Books books2 = dao.saveBooks(books);
		ResponseStructure<Books> structure = new ResponseStructure<Books>();
		if (books2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(books2);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Books> getBooksByName(String name) {
		Books books = dao.getBooksByName(name);
		ResponseStructure<Books> structure = new ResponseStructure<Books>();
		if (books != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(books);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<Books> getBooksById(String id) {
		Books books = dao.getBooksById(id);
		ResponseStructure<Books> structure = new ResponseStructure<Books>();
		if (books != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(books);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<List<Books>> getAllBooks(){
		List<Books> books = dao.getAllBooks();
		ResponseStructure<List<Books>> structure = new ResponseStructure<List<Books>>();
		if (books != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(books);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("NO DATA FOUND");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Boolean> deleteBooksById(String id) {
		boolean status = dao.deleteBooksById(id);
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<Books> updateBooksById(String id, Books books) {
		Books books2 = dao.updateBooks(id, books);
		ResponseStructure<Books> structure = new ResponseStructure<Books>();
		if (books2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(books2);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
}
