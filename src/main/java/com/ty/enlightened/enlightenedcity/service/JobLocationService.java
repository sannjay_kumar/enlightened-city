package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.JobLocationDao;
import com.ty.enlightened.enlightenedcity.dto.JobLocation;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class JobLocationService {

	@Autowired
	private JobLocationDao dao;

	public ResponseStructure<JobLocation> saveJobLocation(JobLocation jobLocation)
	{
		JobLocation jobLocation1=dao.saveJobLocation(jobLocation);
		ResponseStructure<JobLocation> rs=new ResponseStructure<JobLocation>();
		rs.setStatus(HttpStatus.OK.value());
		rs.setMessage("success");
		return rs;
		}

	public ResponseStructure<JobLocation> getJobLocation(String id) {
		JobLocation jobLocation1 = dao.getJobLocationById(id);
		ResponseStructure<JobLocation> rs=new ResponseStructure<JobLocation>();
		if(jobLocation1 == null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + id + " does not exist");
		}
		
	}

	public ResponseStructure<JobLocation> updateJobLocationById(JobLocation jobLocation, String id) {
		JobLocation jobLocation1 =dao.getJobLocationById(id);
		ResponseStructure<JobLocation> rs=new ResponseStructure<JobLocation>();
		if (jobLocation1 != null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + id + " does not exist");
		}

	}
	public boolean deleteJobLocationById(String id) {
		return dao.deleteJobLocationById(id);
	}
	}

	