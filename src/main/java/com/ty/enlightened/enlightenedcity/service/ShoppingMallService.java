package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.ShoppingMallDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.ShoppingMalls;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class ShoppingMallService {
	@Autowired
	ShoppingMallDao dao;

	public ResponseStructure<ShoppingMalls> saveMalls(ShoppingMalls shopmall) {
	dao.saveSMalls(shopmall);
	ResponseStructure<ShoppingMalls> response = new ResponseStructure<ShoppingMalls>();
	response.setStatus(HttpStatus.OK.value());
	response.setMessage("Success");
	response.setData(shopmall);
	return response;
	}
	
	public ResponseStructure<ShoppingMalls> getMallById(String id) {
		ShoppingMalls malls=dao.getMallsById(id);
		if(malls!=null) {
		
		ResponseStructure<ShoppingMalls> response = new ResponseStructure<ShoppingMalls>();
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Success");
		response.setData(malls);
		return response;
		}
		else 
			throw new NoIdFoundException();
	}
	public ResponseStructure<List<ShoppingMalls>> getAllMalls(){
		List<ShoppingMalls> li= dao.getAllMalls();
		if(li!=null) {
			ResponseStructure<List<ShoppingMalls>> response = new ResponseStructure<List<ShoppingMalls>>();
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			response.setData(li);
			return response;
			
		}
		else
			throw new NoIdFoundException();
	}
	public ResponseStructure<ShoppingMalls> updateMallsById(String id,ShoppingMalls malls) {
		ShoppingMalls shopping=dao.getMallsById(id);
		if(shopping!=null) {
			dao.updateMallById(id, malls);
			ResponseStructure<ShoppingMalls> response = new ResponseStructure<ShoppingMalls>();
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			response.setData(malls);
			return response;
		}
		else
			throw new NoIdFoundException();
		
	}
	public ResponseStructure<ShoppingMalls> deleteMallById(String id) {
		ShoppingMalls malls=dao.getMallsById(id);
		if(malls!=null) {
		dao.deleteMallById(id);
		ResponseStructure<ShoppingMalls> response = new ResponseStructure<ShoppingMalls>();
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Success");
		return response;
		}
		else 
			throw new NoIdFoundException();
	}

}
