package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.CompanyDao;
import com.ty.enlightened.enlightenedcity.dto.Company;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class CompanyService {

	@Autowired
	private CompanyDao dao;
public ResponseStructure<Company> saveCompany(Company company)
{
	Company company1=dao.saveCompany(company);
	ResponseStructure<Company> rs=new ResponseStructure<Company>();
	rs.setStatus(HttpStatus.OK.value());
	rs.setMessage("success");

	return rs;
	}

	public ResponseStructure<Company> getCompanyById(String cId) {
		Company company = dao.getCompanyById(cId);
		ResponseStructure<Company> rs=new ResponseStructure<Company>();
		if(company == null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("success");
			
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + cId + " does not exist");
		}
		
	}

	public ResponseStructure<Company> updateCompanyById(Company company, String cId) {
		Company company1=dao.updateCompanyById(company, cId);
		ResponseStructure<Company> rs=new ResponseStructure<Company>();
		if (company1 != null) {
			rs.setStatus(HttpStatus.OK.value());
			rs.setMessage("updated");
			
		
			return rs;
		}
		else {
			throw new NoIdFoundException("given " + cId + " does not exist");	
		}

	}

	public ResponseStructure<Company> deleteCompanyById(String cId) {
	boolean res=dao.deleteCompanyById(cId);
	ResponseStructure<Company> rs=new ResponseStructure<Company>();
	if(res==true)
	{
		rs.setStatus(HttpStatus.OK.value());
		rs.setMessage("deleted");
	    

	
	return rs;
	
	}
	else {
		throw new NoIdFoundException("given " + cId + " does not exist");	
	}
	}

}
