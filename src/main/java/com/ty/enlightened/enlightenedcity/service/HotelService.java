package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.ty.enlightened.enlightenedcity.dao.HotelDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;
import com.ty.enlightened.enlightenedcity.dto.Hotel;
@Service
public class HotelService {
	@Autowired
	HotelDao hdao;
	
	public ResponseStructure<Hotel> saveHotel(Hotel hotel) {
	    hdao.saveHotel(hotel);
		ResponseStructure<Hotel> response = new ResponseStructure<Hotel>();
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Success");
		response.setData(hotel);
		return response;
		
	}
	public ResponseStructure<Hotel> getHotelById(String hotelid) {
		 Hotel hotel=hdao.hotelById(hotelid);
		 ResponseStructure<Hotel> response = new ResponseStructure<Hotel>();
		 if(hotel!=null) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			response.setData(hotel);
			return response;
		 }
		 throw new NoIdFoundException();
	}
	public ResponseStructure<List<Hotel>> getAllHotelByArea(String area) {
		List<Hotel> li= hdao.getAllHotel(area);
		 ResponseStructure<List<Hotel>> response = new ResponseStructure<List<Hotel>>();
		 if(li!=null) {
			 response.setStatus(HttpStatus.OK.value());
				response.setMessage("Success");
				response.setData(li);
				return response;
			 
		 }
		 throw new NoIdFoundException();
	}
	public ResponseStructure<Hotel> deleteHotel(String hotelid) {
		if(hotelid!=null) {
		hdao.deleteHotel(hotelid);
		ResponseStructure<Hotel> response = new ResponseStructure<Hotel>();
		 response.setStatus(HttpStatus.OK.value());
			response.setMessage("Success");
			
			return response;
		}
		 throw new NoIdFoundException();
	}
	public ResponseStructure<Hotel> updateHotel(String hotelid,Hotel hotel) {
	    hdao.updateHotel(hotelid,hotel);
	    if(hotelid!=null) {
		ResponseStructure<Hotel> response = new ResponseStructure<Hotel>();
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Success");
		response.setData(hotel);
		return response;
	    }
	    throw new NoIdFoundException();
		
	}
	
}
