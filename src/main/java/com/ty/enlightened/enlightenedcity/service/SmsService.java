package com.ty.enlightened.enlightenedcity.service;

import java.text.ParseException;

import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import com.ty.enlightened.enlightenedcity.dto.Sms;
import com.ty.enlightened.enlightenedcity.dto.StoredOTP;

@Component
public class SmsService {
	private final String ACCOUNT_SID = "ACd13b7d9f0aae5fb96e94d1053968bae5";

	private final String AUTH_TOKEN = "147ee07d4cdac5f830ca5a4786d62a52";

	private final String FROM_NUMBER = "+18637774756";

	public void send(Sms sms) throws ParseException {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

		int min = 100000;
		int max = 999999;
		int number = (int) (Math.random() * (max - min + 1) + min);

		String msg = "Your OTP - " + number
				+ " please verify this OTP to login --Enlightened City--";

		Message message = Message.creator(new PhoneNumber(sms.getPhoneNo()), new PhoneNumber(FROM_NUMBER), msg)
				.create();
		StoredOTP.setOtp(number);
	}

	public void receive(MultiValueMap<String, String> smscallback) {

	}

}
