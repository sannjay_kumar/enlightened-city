package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.StudentDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Student;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class StudentService {
	@Autowired
	private StudentDao dao;
	
	public ResponseStructure<Student> saveStudent(Student student) {
		Student student2 = dao.saveStudent(student);
		ResponseStructure<Student> structure = new ResponseStructure<Student>();
		if (student2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(student2);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Student> getStudentById(String id) {
		Student student = dao.getStudentById(id);
		ResponseStructure<Student> structure = new ResponseStructure<Student>();
		if (student != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(student);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<List<Student>> getAllStudents(){
		List<Student> students = dao.getAllStudents();
		ResponseStructure<List<Student>> structure = new ResponseStructure<List<Student>>();
		if (students != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(students);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("DATA NOT FOUND");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Boolean> deleteStudentById(String id) {
		boolean status = dao.deleteStudentById(id);
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<Student> updateStudentById(String id, Student student) {
		Student student2 = dao.updateStudent(id, student);
		ResponseStructure<Student> structure = new ResponseStructure<Student>();
		if (student2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(student2);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
}
