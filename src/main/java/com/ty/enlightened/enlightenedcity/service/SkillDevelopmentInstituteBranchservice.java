package com.ty.enlightened.enlightenedcity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.SkillDevelopmentInstituteBranchDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.SkillDevelopmentInstituteBranch;

@Service
public class SkillDevelopmentInstituteBranchservice {
	@Autowired
	private SkillDevelopmentInstituteBranchDao skillDevelopmentInstituteBranchDao ;
	public ResponseStructure<SkillDevelopmentInstituteBranch> saveSkillDevelopmentInstituteBranch(SkillDevelopmentInstituteBranch developmentInstitute) {
		SkillDevelopmentInstituteBranch SkillDevelopmentInstituteBranch = skillDevelopmentInstituteBranchDao.saveSkillDevelopmentInstituteBranch(developmentInstitute);
		ResponseStructure<SkillDevelopmentInstituteBranch> structure =new ResponseStructure<SkillDevelopmentInstituteBranch>();
		if(SkillDevelopmentInstituteBranch !=null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("sucess");
			structure.setData(SkillDevelopmentInstituteBranch);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("failed");
			structure.setData(null);
			
			
		}
		return  structure ;
	}
	public ResponseStructure<SkillDevelopmentInstituteBranch> UpdateSkillDevelopmentInstituteBranchById(String id) {
		SkillDevelopmentInstituteBranch SkillDevelopmentInstituteBranch = skillDevelopmentInstituteBranchDao.getSkillDevelopmentInstituteBranchById(id);
		ResponseStructure<SkillDevelopmentInstituteBranch> structure = new ResponseStructure<SkillDevelopmentInstituteBranch>();
		if(SkillDevelopmentInstituteBranch !=null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("sucess");
			structure.setData(SkillDevelopmentInstituteBranch);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("failed");
			structure.setData(null);
			
			
		}
		return  structure;
	}
	public ResponseStructure<SkillDevelopmentInstituteBranch> getSkillDevelopmentInstituteBranchById(String id) {
		SkillDevelopmentInstituteBranch SkillDevelopmentInstituteBranch = skillDevelopmentInstituteBranchDao.getSkillDevelopmentInstituteBranchById(id);
		ResponseStructure<SkillDevelopmentInstituteBranch> structure = new ResponseStructure<SkillDevelopmentInstituteBranch>();
		if(SkillDevelopmentInstituteBranch !=null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("sucess");
			structure.setData(SkillDevelopmentInstituteBranch);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("failed");
			structure.setData(null);
			
			
		}
		return  structure;
		
	
		}
//	public List<SkillDevelopmentInstituteBranch>getAllSkillDevelopmentInstituteBranchs(){
//		return SkillDevelopmentInstituteBranchDao.getAllSkillDevelopmentInstituteBranch();
	
	public  ResponseStructure<Boolean>SkillDevelopmentInstituteBranchservice(String id){
		Boolean status = skillDevelopmentInstituteBranchDao.deleteSkillDevelopmentInstituteBranchById(id);
		
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("ID DATA FOUND");
			structure.setData(status);
		}
		return structure;
		
		

}
}
