package com.ty.enlightened.enlightenedcity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.enlightened.enlightenedcity.dao.AdministratorDao;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Administrator;
import com.ty.enlightened.enlightenedcity.exception.NoIdFoundException;

@Service
public class AdministratorService {
	@Autowired
	private AdministratorDao dao;
	
	public ResponseStructure<Administrator> saveAdministrator(Administrator administrator) {
		Administrator administrator2 = dao.saveAdministrator(administrator);
		ResponseStructure<Administrator> structure = new ResponseStructure<Administrator>();
		if (administrator2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(administrator2);
		} else {
			structure.setStatus(HttpStatus.BAD_REQUEST.value());
			structure.setMessage("Failed");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Administrator> getAdministratorById(String id) {
		Administrator administrator = dao.getAdministratorById(id);
		ResponseStructure<Administrator> structure = new ResponseStructure<Administrator>();
		if (administrator != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(administrator);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<List<Administrator>> getAllAdministrators(){
		List<Administrator> administrators = dao.getAllAdministrators();
		ResponseStructure<List<Administrator>> structure = new ResponseStructure<List<Administrator>>();
		if (administrators != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(administrators);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("DATA NOT FOUND");
			structure.setData(null);
		}
		return structure;
	}
	
	public ResponseStructure<Boolean> deleteAdministratorById(String id) {
		boolean status = dao.deleteAdministratorById(id);
		ResponseStructure<Boolean> structure = new ResponseStructure<Boolean>();
		if (status) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(status);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
	
	public ResponseStructure<Administrator> updateAdministratorById(String id, Administrator administrator) {
		Administrator administrator2 = dao.updateAdministrator(id, administrator);
		ResponseStructure<Administrator> structure = new ResponseStructure<Administrator>();
		if (administrator2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Success");
			structure.setData(administrator2);
		} else {
			throw new NoIdFoundException();
		}
		return structure;
	}
}
