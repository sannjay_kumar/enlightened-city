package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.Library;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.LibraryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class LibraryController {
	@Autowired
	private LibraryService service;
	
	@ApiOperation(value = "save the library details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "library saved"),
		@ApiResponse(code = 405, message = "Bad request, no proper library details")
	})
	@PostMapping("/library")
	public ResponseStructure<Library> saveLibrary(@RequestBody Library library) {
		return service.saveLibrary(library);
	}
	
	@ApiOperation(value = "get library details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "library data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/library")
	public ResponseStructure<Library> getLibraryById(@RequestParam String id) {
		return service.getLibraryById(id);
	}
	
	@ApiOperation(value = "get all library details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "all library data found"),
		@ApiResponse(code = 404, message = "Not Found, no data found")
	})
	@GetMapping("/alllibrary")
	public ResponseStructure<List<Library>> getAllLibrarys(){
		return service.getAllLibrarys();
	}
	
	@ApiOperation(value = "delete library data", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "library data deleted"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@DeleteMapping("/library")
	public ResponseStructure<Boolean> deleteLibraryById(String id) {
		return service.deleteLibraryById(id);
	}
	
	@ApiOperation(value = "update library details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "library updated"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@PutMapping("/library")
	public ResponseStructure<Library> updateLibraryById(@RequestParam String id, @RequestBody Library library) {
		return service.updateLibraryById(id, library);
	}
}
