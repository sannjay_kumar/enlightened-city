package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Hotel;
import com.ty.enlightened.enlightenedcity.service.HotelService;

@RestController
public class HotelController {
	@Autowired
	HotelService service;
	
	@PostMapping("/hotel")
	public ResponseStructure<Hotel> saveHotel(@RequestBody Hotel hotel){
		return service.saveHotel(hotel);
	}
	@GetMapping("/{hotelid}/hotel")
	public ResponseStructure<Hotel> getHotelById(@PathVariable String hotelid) {
		return service.getHotelById(hotelid);
	}
    @GetMapping("/hotelall")
    public ResponseStructure<List<Hotel>> getAllHotel(@RequestParam String area){
    	return service.getAllHotelByArea(area);
    }
    
    @PutMapping("/hotel")
    public ResponseStructure<Hotel> updateMalls(@RequestParam String id,@RequestBody Hotel hotel) {
    	return service.updateHotel(id,hotel);
    }
    @DeleteMapping("/hoteldel")
    public ResponseStructure<Hotel> deleteHotelId(@RequestParam String hotelid) {
    	return service.deleteHotel(hotelid);
    }
    
}
