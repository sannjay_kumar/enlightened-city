package com.ty.enlightened.enlightenedcity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.Company;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.CompanyService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class CompanyController {

	@Autowired
	private CompanyService companyService;
	@ApiOperation(value="save the Company details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "Company saved"),
		@ApiResponse(code=405, message="Bad Request , unproper industry data") })

	
	@PostMapping("/company")
	public ResponseStructure<Company> saveCompany(@RequestBody Company company) {
		return companyService.saveCompany(company);
	}
	
	@ApiOperation(value="update the Company details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "Company updated"),
		@ApiResponse(code=405, message="Bad Request , unproper industry data") })

	@PutMapping("/company")
	public ResponseStructure<Company> updateCompany(@RequestBody Company company,@RequestParam String cId) {
		return companyService.updateCompanyById(company, cId);
	}
	
	@ApiOperation(value="get the Company details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "Industry got"),
		@ApiResponse(code=405, message="Bad Request , unproper industry data") })

	@GetMapping("/company")
	public ResponseStructure<Company> getCompanyById(@RequestParam String cId) {
		return companyService.getCompanyById(cId);
	}
	
	
	
}
