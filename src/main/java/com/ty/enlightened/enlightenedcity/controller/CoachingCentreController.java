package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.CoachingCentre;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.CoachingCentreservice;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class CoachingCentreController {
	@Autowired
	private CoachingCentreservice coachingCentreservice;
	
	@ApiOperation(value = "save the CoachingCentre details",produces = "CoachingCentreapplication/json",consumes = "CoachingCentreapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "CoachingCentre saved"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentre data")
	})
	@PostMapping("/CoachingCentre")
	public ResponseStructure<CoachingCentre> saveCoachingCentre(@RequestBody @Valid CoachingCentre coachingCentre) {
		return coachingCentreservice.saveCoachingCentre(coachingCentre);
	}
	
	
	
	
	@ApiOperation(value = "update CoachingCentre details",produces = "updateCoachingCentreapplication/json",consumes = "updateCoachingCentreapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "CoachingCentre updated"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentre data")
	})
	@PutMapping("/CoachingCentre")
	public ResponseStructure<CoachingCentre> updateCoachingCentre(@RequestParam String uid, @RequestBody CoachingCentre CoachingCentre) {
		return coachingCentreservice.UpdatecoachingCentreById(CoachingCentre, uid);
	}
     
	@ApiOperation(value = "get CoachingCentre details",produces = "getCoachingCentreapplication/json",consumes = "getCoachingCentreapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "get CoachingCentre by id"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentre data")
	})
	@GetMapping("/CoachingCentre")
	public ResponseStructure<CoachingCentre> getCoachingCentreById(@RequestParam String uid) {
		return coachingCentreservice.getCoachingCentreById(uid);
	}

	@ApiOperation(value = "get all CoachingCentre details",produces = "getallCoachingCentreapplication/json",consumes = "getallCoachingCentreapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "get  all CoachingCentre"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentre data")
	})
	@GetMapping("/getallCoachingCentre")
	public ResponseStructure<List<CoachingCentre>> getallCoachingCentre() {
		return coachingCentreservice.getAllCoachingCentres();
	}
	
	@ApiOperation(value = "delete CoachingCentre details",produces = "deleteCoachingCentreapplication/json",consumes = "deleteCoachingCentreapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = " CoachingCentre deleted"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentre data")
	})

	@DeleteMapping("/CoachingCentre")
	public ResponseStructure<Boolean> deleteCoachingCentre(@RequestParam String uid) {
		return coachingCentreservice.deleteCoachingCentreById(uid);
	}
	

}
