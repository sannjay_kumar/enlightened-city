package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Booking;

import com.ty.enlightened.enlightenedcity.service.BookingService;

@RestController
public class BookingController {
	@Autowired
    BookingService service;
	
	@PostMapping("/{hotelid}/booking")
	public ResponseStructure<Booking> saveBooking(@PathVariable String hotelid, @RequestBody Booking booking) {
		return service.saveBooking(hotelid, booking);
	}
	@GetMapping("/booking")
	public ResponseStructure<Booking> getBookingById(@RequestParam String bid) {
		return service.getBookingId(bid);
	}
    @GetMapping("/bookingall")
    public ResponseStructure<List<Booking>> getAllBooking(@RequestParam String bookid){
    	return service.getAllBooking(bookid);
    }
    @DeleteMapping("/booking")
    public ResponseStructure<Booking> deleteBookingId(@RequestParam String bookingid) {
    	return service.deleteBooking(bookingid);
    }
    @PutMapping("/booking")
    public ResponseStructure<Booking> updateBooking(@RequestParam String id,@RequestBody Booking booking) {
    	return service.updateBooking(id, booking);	
    }

}
