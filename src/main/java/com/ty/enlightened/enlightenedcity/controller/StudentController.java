package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Student;
import com.ty.enlightened.enlightenedcity.service.StudentService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class StudentController {
	@Autowired
	private StudentService service;
	
	@ApiOperation(value = "save the student details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "student saved"),
		@ApiResponse(code = 405, message = "Bad request, no proper student details")
	})
	@PostMapping("/student")
	public ResponseStructure<Student> saveStudent(@RequestBody Student student) {
		return service.saveStudent(student);
	}
	
	@ApiOperation(value = "get student details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "student data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/student")
	public ResponseStructure<Student> getStudentById(@RequestParam String id) {
		return service.getStudentById(id);
	}
	
	@ApiOperation(value = "get all student details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "all student data found"),
		@ApiResponse(code = 404, message = "Not Found, no data found")
	})
	@GetMapping("/allstudent")
	public ResponseStructure<List<Student>> getAllStudents(){
		return service.getAllStudents();
	}
	
	@ApiOperation(value = "delete student data", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "student data deleted"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@DeleteMapping("/student")
	public ResponseStructure<Boolean> deleteStudentById(@RequestParam String id) {
		return service.deleteStudentById(id);
	}
	
	@ApiOperation(value = "update student details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "student updated"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@PutMapping("/student")
	public ResponseStructure<Student> updateStudentById(@RequestParam String id, @RequestBody Student student) {
		return service.updateStudentById(id, student);
	}
}
