package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RestController;


import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.ShoppingMalls;
import com.ty.enlightened.enlightenedcity.service.ShoppingMallService;

@RestController
public class ShoppingMallContrller {
	@Autowired
	ShoppingMallService service;
	
	@PostMapping("/mall")
	public ResponseStructure<ShoppingMalls> saveShoppingMalls(@RequestBody ShoppingMalls malls) {
		return service.saveMalls(malls);
	}
	@GetMapping("/mall")
	public ResponseStructure<ShoppingMalls> getMallById(@RequestParam String mallid) {
		return service.getMallById(mallid);
	}
    @GetMapping("/allmall")
    public ResponseStructure<List<ShoppingMalls>> getAllMall(){
    	return service.getAllMalls();
    }
    @PutMapping("/mall")
    public ResponseStructure<ShoppingMalls> updateMalls(@RequestParam String id,@RequestBody ShoppingMalls malls) {
    	return service.updateMallsById(id, malls);
    }
    @DeleteMapping("/mall")
    public ResponseStructure<ShoppingMalls> deleteBookingId(@RequestParam String mallid) {
         return service.deleteMallById(mallid);
    }
}
