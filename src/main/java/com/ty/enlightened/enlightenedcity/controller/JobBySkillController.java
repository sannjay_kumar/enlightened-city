package com.ty.enlightened.enlightenedcity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.JobBySkill;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.JobBySkillService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class JobBySkillController {

	@Autowired
	private JobBySkillService jobBySkillService;

	@ApiOperation(value = "save the JobBySkill details", produces = "application/json", consumes = "application/json")
	@ApiResponses({ @ApiResponse(code = 200, message = "JobBySkill saved"),
			@ApiResponse(code = 405, message = "Bad Request , unproper jobBySkill data") })

	@PostMapping("/jobBySkill")
	public ResponseStructure<JobBySkill> saveJobBySkill(@RequestBody JobBySkill jobBySkill) {
		return jobBySkillService.saveJobBySkill(jobBySkill);
	}

	@ApiOperation(value = "update the JobBySkill details", produces = "application/json", consumes = "application/json")
	@ApiResponses({ @ApiResponse(code = 200, message = "JobBySkill updated"),
			@ApiResponse(code = 405, message = "Bad Request , unproper jobBySkill data") })

	@PutMapping("/jobBySkill")
	public ResponseStructure<JobBySkill> updateJobBySkill(@RequestBody JobBySkill jobBySkill, @RequestParam String id) {
		return jobBySkillService.updateJobBySkillById(jobBySkill, id);
	}

	@ApiOperation(value = "get the JobBySkill details", produces = "application/json", consumes = "application/json")
	@ApiResponses({ @ApiResponse(code = 200, message = " get the JobBySkill"),
			@ApiResponse(code = 405, message = "Bad Request , unproper jobBySkill id") })

	@GetMapping("/jobBySkill")
	public ResponseStructure<JobBySkill> getindJobBySkillById(@RequestParam String id) {
		return jobBySkillService.getJobBySkill(id);
	}

	@DeleteMapping("/jobBySkill")
	public boolean deleteJobBySkillById(@RequestParam String id) {
		return jobBySkillService.deleteJobBySkillById(id);

	}
}