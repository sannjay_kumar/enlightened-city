package com.ty.enlightened.enlightenedcity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.JobLocation;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.JobLocationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class JobLocationController {

	@Autowired
	private JobLocationService jobByLocationService;

	@ApiOperation(value = "save the  JobLocation details", produces = "application/json", consumes = "application/json")
	@ApiResponses({ @ApiResponse(code = 200, message = " JobLocation saved"),
			@ApiResponse(code = 405, message = "Bad Request , unproper  jobLocation data") })

	@PostMapping("/ jobLocation")
	public ResponseStructure<JobLocation> saveJobLocation(@RequestBody JobLocation jobLocation) {
		return jobByLocationService.saveJobLocation(jobLocation);
	}

	@ApiOperation(value = "update the  JobLocation details", produces = "application/json", consumes = "application/json")
	@ApiResponses({ @ApiResponse(code = 200, message = " JobLocation updated"),
			@ApiResponse(code = 405, message = "Bad Request , unproper  jobLocation data") })

	@PutMapping("/ jobLocation")
	public ResponseStructure<JobLocation> updateJobLocation(@RequestBody JobLocation jobLocation,
			@RequestParam String id) {
		return jobByLocationService.updateJobLocationById(jobLocation, id);
	}

	@ApiOperation(value = "get the  JobLocation details", produces = "application/json", consumes = "application/json")
	@ApiResponses({ @ApiResponse(code = 200, message = " get the  JobLocation"),
			@ApiResponse(code = 405, message = "Bad Request , unproper  jobLocation id") })

	@GetMapping("/ jobLocation")
	public ResponseStructure<JobLocation> getJobLocationById(@RequestParam String id) {
		return jobByLocationService.getJobLocation(id);
	}

}
