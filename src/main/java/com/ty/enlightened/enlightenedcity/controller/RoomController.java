package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.Room;
import com.ty.enlightened.enlightenedcity.service.RoomService;

@RestController
public class RoomController {
	@Autowired
	private RoomService roomService;
	
	@PostMapping("/room")
	public ResponseStructure<Room> saveRoom(@RequestParam String bid,@RequestBody Room room) {
		
		return roomService.saveRoom(bid,room);
	}
	@GetMapping("/room")
	public ResponseStructure<Room> getRoomById(@RequestParam String rid){
		return roomService.getRoomId(rid);
	}
	@GetMapping("/allroom")
	public ResponseStructure<List<Room>> getAllRoom(){
		return roomService.getAllRoom();
	}
	
	@PutMapping("/room")
	public ResponseStructure<Room> updateRoomById(@RequestParam String rid,@RequestBody Room room){
		return roomService.updateRoom(rid, room);
	}
	
	@DeleteMapping("/room")
	public ResponseStructure<Boolean> deleteRoomById(@RequestParam String rid){
		return deleteRoomById(rid);
	}
	

}
