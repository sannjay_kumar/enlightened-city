package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.TouristPlaces;
import com.ty.enlightened.enlightenedcity.service.TouristPlacesService;
@RestController
public class TouristPlacesController {
	@Autowired
	private TouristPlacesService placesService;
	
	@PostMapping("/tourist")
	public ResponseStructure<TouristPlaces> savePlaces(@RequestBody TouristPlaces touristPlaces) {
		return placesService.saveTouristPlaces(touristPlaces);
		
	}
	
	@GetMapping("/tourist")
	public ResponseStructure<TouristPlaces>  getPlacesById(@RequestParam String tId) {
		return placesService.getTouristById(tId);
	}
	
	@GetMapping("/alltourist")
	public ResponseStructure<List<TouristPlaces>> allTourist() {
		return placesService.allTourist();
	}
	
	@PutMapping("/tourist")
	public ResponseStructure<TouristPlaces> updateTouristPlacesById(@RequestParam String id, @RequestBody TouristPlaces touristPlaces){
		return placesService.updatetouristById(id, touristPlaces);
	}
	
	@DeleteMapping("/tourist")
	public ResponseStructure<TouristPlaces>  deletePlacesById(@RequestParam String tId) {
		 return placesService.deleteTouristPlacesById(tId);
	}
}
