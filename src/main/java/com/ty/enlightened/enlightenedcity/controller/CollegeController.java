package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.College;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.CollegeService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class CollegeController {
	@Autowired
	private CollegeService service;
	
	@ApiOperation(value = "save the college details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "college saved"),
		@ApiResponse(code = 405, message = "Bad request, no proper college details")
	})
	@PostMapping("/college")
	public ResponseStructure<College> saveCollege(@RequestBody College college) {
		return service.saveCollege(college);
	}
	
	@ApiOperation(value = "get college details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "college data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/collegebyname")
	public ResponseStructure<College> getCollegeByName(@RequestParam String name) {
		return service.getCollegeByName(name);
	}
	
	@ApiOperation(value = "get college details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "college data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/college")
	public ResponseStructure<College> getCollegeById(@RequestParam String id) {
		return service.getCollegeById(id);
	}
	
	@ApiOperation(value = "get all college details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "all college data found"),
		@ApiResponse(code = 404, message = "Not Found, no data found")
	})
	@GetMapping("/allcollege")
	public ResponseStructure<List<College>> getAllColleges(){
		return service.getAllColleges();
	}
	
	@ApiOperation(value = "delete college data", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "college data deleted"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@DeleteMapping("/college")
	public ResponseStructure<Boolean> deleteCollegeById(String id) {
		return service.deleteCollegeById(id);
	}
	
	@ApiOperation(value = "update college details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "college updated"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@PutMapping("/college")
	public ResponseStructure<College> updateCollegeById(@RequestParam String id, @RequestBody College college) {
		return service.updateCollegeById(id, college);
	}
}
