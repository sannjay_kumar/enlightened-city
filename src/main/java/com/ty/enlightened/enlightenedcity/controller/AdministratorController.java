package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.Administrator;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.AdministratorService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class AdministratorController {
	@Autowired
	private AdministratorService administratorService;
	
	@ApiOperation(value = "save the Administrator details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Administrator saved"),
		@ApiResponse(code = 405, message = "Bad request, no proper Administrator details")
	})
	@PostMapping("/Administrator")
	public ResponseStructure<Administrator> saveAdministrator(@RequestBody Administrator administrator) {
		return administratorService.saveAdministrator(administrator);
	}
	
	@ApiOperation(value = "get Administrator details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Administrator data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/Administrator")
	public ResponseStructure<Administrator> getAdministratorById(@RequestParam String id) {
		return administratorService.getAdministratorById(id);
	}
	
	@ApiOperation(value = "get all Administrator details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "all Administrator data found"),
		@ApiResponse(code = 404, message = "Not Found, no data found")
	})
	@GetMapping("/allAdministrator")
	public ResponseStructure<List<Administrator>> getAllAdministrators(){
		return administratorService.getAllAdministrators();
	}
	
	@ApiOperation(value = "delete Administrator data", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Administrator data deleted"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@DeleteMapping("/Administrator")
	public ResponseStructure<Boolean> deleteAdministratorById(@RequestParam String id) {
		return administratorService.deleteAdministratorById(id);
	}
	
	@ApiOperation(value = "update Administrator details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Administrator updated"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@PutMapping("/Administrator")
	public ResponseStructure<Administrator> updateAdministratorById(@RequestParam String id, @RequestBody Administrator administrator) {
		return administratorService.updateAdministratorById(id, administrator);
	}
}
