package com.ty.enlightened.enlightenedcity.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.SkillDevelopmentInstitute;
import com.ty.enlightened.enlightenedcity.service.SkillDevelopmentInstituteservice;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@RestController
public class SkillDevelopmentInstitueBranchController {
	@Autowired
	private SkillDevelopmentInstituteservice skillDevelopmentInstituteservice;
	
	@ApiOperation(value = "save the SkillDevelopmentInstitute details",produces = "SkillDevelopmentInstituteapplication/json",consumes = "SkillDevelopmentInstituteapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "SkillDevelopmentInstitute saved"),
		@ApiResponse(code = 405,message = "bad request,not proper SkillDevelopmentInstitute data")
	})
	@PostMapping("/skillDevelopmentInstitute")
	public ResponseStructure<SkillDevelopmentInstitute> saveSkillDevelopmentInstitute(@RequestBody @Valid SkillDevelopmentInstitute skillDevelopmentInstitute) {
		return skillDevelopmentInstituteservice.saveSkillDevelopmentInstitute(skillDevelopmentInstitute);
	}
	
	
	
	
	@ApiOperation(value = "update SkillDevelopmentInstitute details",produces = "updateSkillDevelopmentInstituteapplication/json",consumes = "updateSkillDevelopmentInstituteapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "SkillDevelopmentInstitute updated"),
		@ApiResponse(code = 405,message = "bad request,not proper SkillDevelopmentInstitute data")
	})
	@PutMapping("/skillDevelopmentInstituteBranch")
	public ResponseStructure<SkillDevelopmentInstitute> updateSkillDevelopmentInstitute(@RequestParam String uid, @RequestBody SkillDevelopmentInstitute skillDevelopmentInstitute) {
		return skillDevelopmentInstituteservice.UpdateSkillDevelopmentInstituteById(uid);
	}
     
	@ApiOperation(value = "get SkillDevelopmentInstitute details",produces = "getSkillDevelopmentInstituteapplication/json",consumes = "getSkillDevelopmentInstituteapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "get SkillDevelopmentInstitute by id"),
		@ApiResponse(code = 405,message = "bad request,not proper SkillDevelopmentInstitute data")
	})
	@GetMapping("/skillDevelopmentInstituteBranch")
	public ResponseStructure<SkillDevelopmentInstitute> getSkillDevelopmentInstituteById(@RequestParam String uid) {
		return skillDevelopmentInstituteservice.getSkillDevelopmentInstituteById(uid);
	}

//	@ApiOperation(value = "get all SkillDevelopmentInstitute details",produces = "getallSkillDevelopmentInstituteapplication/json",consumes = "getallSkillDevelopmentInstituteapplication/json")
//	@ApiResponses({
//		@ApiResponse(code = 200,message = "get  all SkillDevelopmentInstitute"),
//		@ApiResponse(code = 405,message = "bad request,not proper SkillDevelopmentInstitute data")
//	})
////	@GetMapping("/getallSkillDevelopmentInstitute")
//	public ResponseStructure<List<SkillDevelopmentInstitute>> getallSkillDevelopmentInstitute() {
//		return skillDevelopmentInstituteservice.
//	}
	
	@ApiOperation(value = "delete SkillDevelopmentInstitute details",produces = "deleteSkillDevelopmentInstituteapplication/json",consumes = "deleteSkillDevelopmentInstituteapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = " SkillDevelopmentInstitute deleted"),
		@ApiResponse(code = 405,message = "bad request,not proper SkillDevelopmentInstitute data")
	})

	@DeleteMapping("/skillDevelopmentInstituteBranch")
	public ResponseStructure<Boolean> SkillDevelopmentInstituteserviceById(@RequestParam String uid) {
		return  skillDevelopmentInstituteservice.SkillDevelopmentInstituteserviceById(uid);
	}
	

}
