package com.ty.enlightened.enlightenedcity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.JobSeeker;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.JobSeekerService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@RestController
public class JobBySeekerController {

	@Autowired
	private JobSeekerService jobSeekerService;

	@ApiOperation(value="save the JobSeeker details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "JobSeeker saved"),
		@ApiResponse(code=405, message="Bad Request , unproper jobSeeker data") })

	@PostMapping("/jobSeeker")
	public ResponseStructure<JobSeeker> saveJobSeeker(@RequestBody JobSeeker jobSeeker) {
		return jobSeekerService.saveJobSeeker(jobSeeker);
	}
	@ApiOperation(value="update the JobSeeker details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "JobSeeker updated"),
		@ApiResponse(code=405, message="Bad Request , unproper jobSeeker data") })
	
	@PutMapping("/jobSeeker")
	public ResponseStructure<JobSeeker> updateJobSeeker(@RequestBody JobSeeker jobSeeker,@RequestParam String sId) {
		return jobSeekerService.updateJobSeekerById(jobSeeker, sId);
	}                 
	 
	@ApiOperation(value="get the JobSeeker details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= " get the JobSeeker"),
		@ApiResponse(code=405, message="Bad Request , unproper jobSeeker sId") })
	
	@GetMapping("/jobSeeker")
	public ResponseStructure<JobSeeker> getindJobSeekerById(@RequestParam String sId) {
		return jobSeekerService.getJobSeeker(sId);
	}
	


	@DeleteMapping("/jobSeeker")
	public boolean deleteJobSeekerById(@RequestParam String sId) {
		return jobSeekerService.deleteJobSeekerById(sId);

	}

}
