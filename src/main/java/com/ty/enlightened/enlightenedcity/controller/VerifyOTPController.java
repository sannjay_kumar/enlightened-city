package com.ty.enlightened.enlightenedcity.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.StoredOTP;
import com.ty.enlightened.enlightenedcity.dto.TempOTP;

@RestController
public class VerifyOTPController {
	@PostMapping("/otp")
	public String verifyOTP(@RequestBody TempOTP otp) {
		if (otp.getOtp() == StoredOTP.getOtp()) {
			return "Correct OTP";
		}
		return "Incorrect OTP";
	}
}
