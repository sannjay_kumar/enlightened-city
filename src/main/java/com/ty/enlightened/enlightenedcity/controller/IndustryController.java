package com.ty.enlightened.enlightenedcity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.Industry;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.IndustryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class IndustryController {

	@Autowired
	private IndustryService industryService;
	
	@ApiOperation(value="save the Industry details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "Industry saved"),
		@ApiResponse(code=405, message="Bad Request , unproper industry data") })

	@PostMapping("/industry")
	public ResponseStructure<Industry> saveIndustry(@RequestBody Industry industry) {
		return industryService.saveIndustry(industry);
	}
	@ApiOperation(value="update the Industry details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "Industry updated"),
		@ApiResponse(code=405, message="Bad Request , unproper industry data") })
	
	@PutMapping("/industry")
	public ResponseStructure<Industry> updateIndustry(@RequestBody Industry industry,@RequestParam String I_id) {
		return industryService.updateIndustryById(industry, I_id);
	}                 
	 
	@ApiOperation(value="get the Industry details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= " get the Industry"),
		@ApiResponse(code=405, message="Bad Request , unproper industry id") })
	
	@GetMapping("/industry")
	public ResponseStructure<Industry> getindIndustryById(@RequestParam String I_id) {
		return industryService.getIndustryById(I_id);
	}
	
	
	

}
