package com.ty.enlightened.enlightenedcity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.config.Emailconfig;
import com.ty.enlightened.enlightenedcity.dto.Feedback;

@RestController
public class FeedbackController {
	@Autowired
	private Emailconfig config;


	@RequestMapping("/welcome")
	public String welcome() {
		return "welcome to my application";
	}
	@RequestMapping(value="/sendemail",method=RequestMethod.POST)
	public ResponseEntity<?> sendEmail(@RequestBody Feedback feed){
		
		System.out.println(feed);
		boolean resul=this.config.sendemail(feed.getName(),feed.getFeedback(),feed.getTo());
		if(resul) {
			return ResponseEntity.ok("Email is sent");
		}
		else
		{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Email not sent");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
