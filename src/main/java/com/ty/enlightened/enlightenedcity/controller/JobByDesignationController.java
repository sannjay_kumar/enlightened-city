package com.ty.enlightened.enlightenedcity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.JobByDesignation;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.JobByDesignationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class JobByDesignationController {

	@Autowired
	private JobByDesignationService jobByDesignationService;

	@ApiOperation(value="save the JobByDesignation details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "JobByDesignation saved"),
		@ApiResponse(code=405, message="Bad Request , unproper jobByDesignation data") })

	@PostMapping("/jobByDesignation")
	public ResponseStructure<JobByDesignation> saveJobByDesignation(@RequestBody JobByDesignation jobByDesignation) {
		return jobByDesignationService.saveJobByDesignation(jobByDesignation);
	}
	@ApiOperation(value="update the JobByDesignation details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= "JobByDesignation updated"),
		@ApiResponse(code=405, message="Bad Request , unproper jobByDesignation data") })
	
	@PutMapping("/jobByDesignation")
	public ResponseStructure<JobByDesignation> updateJobByDesignation(@RequestBody JobByDesignation jobByDesignation,@RequestParam String id) {
		return jobByDesignationService.updateJobByDesignationById(jobByDesignation, id);
	}                 
	 
	@ApiOperation(value="get the JobByDesignation details", produces="application/json", consumes="application/json")
	@ApiResponses({ @ApiResponse(code=200,message= " get the JobByDesignation"),
		@ApiResponse(code=405, message="Bad Request , unproper jobByDesignation id") })
	
	@GetMapping("/jobByDesignation")
	public ResponseStructure<JobByDesignation> getindJobByDesignationById(@RequestParam String id) {
		return jobByDesignationService.getJobByDesignation(id);
	}
	

}
