package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.University;
import com.ty.enlightened.enlightenedcity.service.UniversityService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class UniversityController {
	@Autowired
	private UniversityService service;
	
	@ApiOperation(value = "save the university details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "university saved"),
		@ApiResponse(code = 405, message = "Bad request, no proper university details")
	})
	@PostMapping("/university")
	public ResponseStructure<University> saveUniversity(@RequestBody University university) {
		return service.saveUniversity(university);
	}
	
	@ApiOperation(value = "get university details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "university data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/university")
	public ResponseStructure<University> getUniversityById(@RequestParam String id) {
		return service.getUniversityById(id);
	}
	
	@ApiOperation(value = "get all university details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "all university data found"),
		@ApiResponse(code = 404, message = "Not Found, no data found")
	})
	@GetMapping("/alluniversity")
	public ResponseStructure<List<University>> getAllUniversitys(){
		return service.getAllUniversitys();
	}
	
	@ApiOperation(value = "delete university data", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "university data deleted"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@DeleteMapping("/university")
	public ResponseStructure<Boolean> deleteUniversityById(String id) {
		return service.deleteUniversityById(id);
	}
	
	@ApiOperation(value = "update university details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "university updated"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@PutMapping("/university")
	public ResponseStructure<University> updateUniversityById(@RequestParam String id, @RequestBody University university) {
		return service.updateUniversityById(id, university);
	}
}
