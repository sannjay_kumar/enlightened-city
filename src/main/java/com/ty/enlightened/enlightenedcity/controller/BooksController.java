package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.Books;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.BooksService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class BooksController {
	@Autowired
	private BooksService service;
	
	@ApiOperation(value = "save books details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "books saved"),
		@ApiResponse(code = 405, message = "Bad request, no proper book details")
	})
	@PostMapping("/books")
	public ResponseStructure<Books> saveBooks(@RequestBody Books books) {
		return service.saveBooks(books);
	}
	
	@ApiOperation(value = "get book details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "book data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/booksbyname")
	public ResponseStructure<Books> getBooksByName(@RequestParam String name) {
		return service.getBooksByName(name);
	}
	
	@ApiOperation(value = "get book details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "book data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/books")
	public ResponseStructure<Books> getBooksById(@RequestParam String id) {
		return service.getBooksById(id);
	}
	
	@ApiOperation(value = "get all books details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "all books data found"),
		@ApiResponse(code = 404, message = "Not Found, no data found")
	})
	@GetMapping("/allbooks")
	public ResponseStructure<List<Books>> getAllBooks(){
		return service.getAllBooks();
	}
	
	@ApiOperation(value = "delete book data", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "book data deleted"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@DeleteMapping("/books")
	public ResponseStructure<Boolean> deleteBooksById(String id) {
		return service.deleteBooksById(id);
	}
	
	@ApiOperation(value = "update book details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "book updated"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@PutMapping("/books")
	public ResponseStructure<Books> updateBooksById(@RequestParam String id, @RequestBody Books books) {
		return service.updateBooksById(id, books);
	}
}
