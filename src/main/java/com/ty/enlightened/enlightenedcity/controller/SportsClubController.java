package com.ty.enlightened.enlightenedcity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.dto.SportsClub;
import com.ty.enlightened.enlightenedcity.service.SportsClubService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class SportsClubController {
	@Autowired
	private SportsClubService service;
	
	@ApiOperation(value = "save the SportsClub details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "SportsClub saved"),
		@ApiResponse(code = 405, message = "Bad request, no proper SportsClub details")
	})
	@PostMapping("/sportsclub")
	public ResponseStructure<SportsClub> saveSportsClub(@RequestBody SportsClub sportsClub) {
		return service.saveSportsClub(sportsClub);
	}
	
	@ApiOperation(value = "get SportsClub details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "SportsClub data found"),
		@ApiResponse(code = 404, message = "Not Found, id not found")
	})
	@GetMapping("/sportsclub")
	public ResponseStructure<SportsClub> getSportsClubById(@RequestParam String id) {
		return service.getSportsClubById(id);
	}
	
	@ApiOperation(value = "get all SportsClub details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "all SportsClub data found"),
		@ApiResponse(code = 404, message = "Not Found, no data found")
	})
	@GetMapping("/allsportsclub")
	public ResponseStructure<List<SportsClub>> getAllSportsClubs(){
		return service.getAllSportsClubs();
	}
	
	@ApiOperation(value = "delete SportsClub data", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "SportsClub data deleted"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@DeleteMapping("/sportsclub")
	public ResponseStructure<Boolean> deleteSportsClubById(@RequestParam String id) {
		return service.deleteSportsClubById(id);
	}
	
	@ApiOperation(value = "update SportsClub details", produces = "application/json", consumes = "application/json")
	@ApiResponses({
		@ApiResponse(code = 200, message = "SportsClub updated"),
		@ApiResponse(code = 405, message = "Bad request, id not found")
	})
	@PutMapping("/SportsClub")
	public ResponseStructure<SportsClub> updateSportsClubById(@RequestParam String id, @RequestBody SportsClub sportsClub) {
		return service.updateSportsClubById(id, sportsClub);
	}
}
