package com.ty.enlightened.enlightenedcity.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.enlightened.enlightenedcity.dto.CoachingCentreBranch;
import com.ty.enlightened.enlightenedcity.dto.ResponseStructure;
import com.ty.enlightened.enlightenedcity.service.CoachingCentreBranchBranchservice;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@RestController
public class CoachingCentreBranchController {
	@Autowired
	private CoachingCentreBranchBranchservice CoachingCentreBranchservice;
	
	@ApiOperation(value = "save the CoachingCentreBranch details",produces = "CoachingCentreBranchapplication/json",consumes = "CoachingCentreBranchapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "CoachingCentreBranch saved"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentreBranch data")
	})
	@PostMapping("/CoachingCentreBranch")
	public ResponseStructure<CoachingCentreBranch> saveCoachingCentreBranch(@RequestBody @Valid CoachingCentreBranch CoachingCentreBranch) {
		return CoachingCentreBranchservice.saveCoachingCentreBranch(CoachingCentreBranch);
	}
	
	
	
	
	@ApiOperation(value = "update CoachingCentreBranch details",produces = "updateCoachingCentreBranchapplication/json",consumes = "updateCoachingCentreBranchapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "CoachingCentreBranch updated"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentreBranch data")
	})
	@PutMapping("/CoachingCentreBranch")
	public ResponseStructure<CoachingCentreBranch> updateCoachingCentreBranch(@RequestParam String uid, @RequestBody CoachingCentreBranch CoachingCentreBranch) {
		return CoachingCentreBranchservice.UpdateCoachingCentreBranchById(CoachingCentreBranch, uid);
	}
     
	@ApiOperation(value = "get CoachingCentreBranch details",produces = "getCoachingCentreBranchapplication/json",consumes = "getCoachingCentreBranchapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = "get CoachingCentreBranch by id"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentreBranch data")
	})
	@GetMapping("/CoachingCentreBranch")
	public ResponseStructure<CoachingCentreBranch> getCoachingCentreBranchById(@RequestParam String uid) {
		return CoachingCentreBranchservice.getCoachingCentreBranchById(uid);
	}

	
	
	@ApiOperation(value = "delete CoachingCentreBranch details",produces = "deleteCoachingCentreBranchapplication/json",consumes = "deleteCoachingCentreBranchapplication/json")
	@ApiResponses({
		@ApiResponse(code = 200,message = " CoachingCentreBranch deleted"),
		@ApiResponse(code = 405,message = "bad request,not proper CoachingCentreBranch data")
	})

	@DeleteMapping("/CoachingCentreBranch")
	public ResponseStructure<Boolean> deleteCoachingCentreBranch(@RequestParam String uid) {
		return CoachingCentreBranchservice.deleteCoachingCentreById(uid);
	}
	
	
	}


