 package com.ty.enlightened.enlightenedcity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class EnlightenedCityApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnlightenedCityApplication.class, args);
	}
	
	@Bean
	public Docket getDocket() {
		Contact contact = new Contact("Enlightened", "www.testyantra.com", "enlightenedcity6@gmail.com");
		List<VendorExtension> extensions = new ArrayList<VendorExtension>();
		ApiInfo apiInfo = new ApiInfo("City API Service", "Service to control city", "SNAPSHOT 1.0", "www.testyantraglobal.com", contact, "Licence 12315", "www.testyantra.com", extensions);
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.ty")).build().apiInfo(apiInfo).useDefaultResponseMessages(false);
	}
}
