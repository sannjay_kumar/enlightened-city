package com.ty.enlightened.enlightenedcity.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.JobSeeker;
import com.ty.enlightened.enlightenedcity.repository.JobSeekerRepository;

@Repository
public class JobSeekerDao {

	
	@Autowired
	private JobSeekerRepository jobseekerepository;

	public JobSeeker saveJobSeeker(JobSeeker jobSeeker) {
		return jobseekerepository.save(jobSeeker);
	}

	public JobSeeker getJobSeekerById(String sId) {
		Optional<JobSeeker> optional = jobseekerepository.findById(sId);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public JobSeeker updateJobSeekerById(JobSeeker jobSeeker, String sId) {
		JobSeeker existingJobSeeker = getJobSeekerById(sId);
		if (existingJobSeeker != null) {
			jobseekerepository.save(jobSeeker);
			return jobSeeker;
		}
		return null;

	}

	public boolean deleteJobSeekerById(String sId) {
		JobSeeker jobSeeker = getJobSeekerById(sId);
		if (jobSeeker != null) {
			jobseekerepository.deleteById(sId);
			return true;
		}
		return false;
	}
}
