package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.Student;
import com.ty.enlightened.enlightenedcity.repository.StudentRepository;

@Repository
public class StudentDao {
	@Autowired
	private StudentRepository repository;
	
	public Student saveStudent(Student student) {
		return repository.save(student);
	}
	
	public Student getStudentById(String id) {
		Optional<Student> optional = repository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	
	public boolean deleteStudentById(String id) {
		Student student = getStudentById(id);
		if (student != null) {
			repository.delete(student);
			return true;
		}
		return false;
	}
	
	public List<Student> getAllStudents(){
		return repository.findAll();
	}
	
	public Student updateStudent(String id, Student student) {
		Student student2 = getStudentById(id);
		if (student2 != null) {
			return repository.save(student);
		}
		return null;
	}
}
