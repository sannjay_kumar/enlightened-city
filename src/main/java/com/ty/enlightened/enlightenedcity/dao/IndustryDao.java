package com.ty.enlightened.enlightenedcity.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.Industry;
import com.ty.enlightened.enlightenedcity.repository.IndustryRepository;

@Repository
public class IndustryDao {

	@Autowired
	private IndustryRepository industryRepository;

	public Industry saveIndustry(Industry industry) {
		return industryRepository.save(industry);
	}

	public Industry getIndustryById(String I_id) {
		Optional<Industry> optional = industryRepository.findById(I_id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public Industry updateIndustryById(Industry industry, String I_id) {
		Industry existingindustry = getIndustryById(I_id);
		if (existingindustry != null) {
			industryRepository.save(industry);
			return industry;
		}
		return null;

	}

	public boolean deleteIndustryById(String I_id) {
		Industry industry = getIndustryById(I_id);
		if (industry != null) {
			industryRepository.deleteById(I_id);
			return true;
		}
		return false;
	}

}
