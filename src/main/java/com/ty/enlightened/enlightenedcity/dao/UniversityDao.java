package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.University;
import com.ty.enlightened.enlightenedcity.repository.UniversityRepository;

@Repository
public class UniversityDao {
	@Autowired
	private UniversityRepository repository;
	
	public University saveUniversity(University university) {
		return repository.save(university);
	}
	
	public University getUniversityById(String id) {
		Optional<University> optional = repository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	
	public List<University> getAllUniversities(){
		return repository.findAll();
	}
	
	public boolean deleteUniversityById(String id) {
		University university = getUniversityById(id);
		if (university != null) {
			repository.delete(university);
			return true;
		}
		return false;
	}
	
	public University updateUniversityById(String id, University university) {
		University extuniversity = getUniversityById(id);
		if (extuniversity != null) {
			return repository.save(university);
		}
		return null;
	}
}
