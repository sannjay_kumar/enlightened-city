package com.ty.enlightened.enlightenedcity.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.SkillDevelopmentInstituteBranch;
import com.ty.enlightened.enlightenedcity.repository.SkillDevelopmentInstituteBranchRepository;
@Repository
public class SkillDevelopmentInstituteBranchDao {
	@Autowired
	private SkillDevelopmentInstituteBranchRepository centreRepository;

	public SkillDevelopmentInstituteBranch saveSkillDevelopmentInstituteBranch(SkillDevelopmentInstituteBranch  skillDevelopmentInstituteBranch) {
		return centreRepository.save(skillDevelopmentInstituteBranch);
	}

	public SkillDevelopmentInstituteBranch updateSkillDevelopmentInstituteBranch(SkillDevelopmentInstituteBranch skillDevelopmentInstituteBranch, String id) {
		SkillDevelopmentInstituteBranch existingSkillDevelopmentInstituteBranch = getSkillDevelopmentInstituteBranchById(id);
		if (existingSkillDevelopmentInstituteBranch != null) {
			centreRepository.save(skillDevelopmentInstituteBranch);
			return skillDevelopmentInstituteBranch;
		}
		return null;
	}

	public SkillDevelopmentInstituteBranch getSkillDevelopmentInstituteBranchById(String id) {
		Optional<SkillDevelopmentInstituteBranch> optional = centreRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	public boolean deleteSkillDevelopmentInstituteBranchById(String id) {
		SkillDevelopmentInstituteBranch skillDevelopmentInstituteBranch = getSkillDevelopmentInstituteBranchById(id);
		if (skillDevelopmentInstituteBranch != null) {
			centreRepository.delete(skillDevelopmentInstituteBranch);
			return true;
		}
		return false;

}
}
