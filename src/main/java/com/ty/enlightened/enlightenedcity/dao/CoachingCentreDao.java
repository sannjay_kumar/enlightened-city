package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.CoachingCentre;
import com.ty.enlightened.enlightenedcity.repository.CoachingCentreRepository;


@Repository
public class CoachingCentreDao {
	@Autowired
	private CoachingCentreRepository centreRepository;

	public CoachingCentre saveCoachingCentr(CoachingCentre coachingCentr) {
		return centreRepository.save(coachingCentr);
	}

	public CoachingCentre updateCoachingCentre(CoachingCentre coachingCentre, String id) {
		CoachingCentre existingCoachingCentre = getCoachingCentreById(id);
		if (existingCoachingCentre != null) {
			centreRepository.save(coachingCentre);
			return coachingCentre;
		}
		return null;
	}

	public CoachingCentre getCoachingCentreById(String id) {
		Optional<CoachingCentre> optional = centreRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public List<CoachingCentre> getAllCoachingCentre() {
		return centreRepository.findAll();
	}

	public boolean deleteCoachingCentreById(String id) {
		CoachingCentre  coachingCentre = getCoachingCentreById(id);
		if (coachingCentre != null) {
			centreRepository.delete(coachingCentre);
			return true;
		}
		return false;

	}
	

}
