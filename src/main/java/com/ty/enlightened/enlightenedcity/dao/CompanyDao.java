package com.ty.enlightened.enlightenedcity.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.Company;
import com.ty.enlightened.enlightenedcity.repository.CompanyRepository;

@Repository
public class CompanyDao {
	@Autowired
	private CompanyRepository companyRepository;

	public Company saveCompany(Company company) {
		return companyRepository.save(company);
	}

	public Company getCompanyById(String cId) {
		Optional<Company> optional = companyRepository.findById(cId);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public Company updateCompanyById(Company company, String cId) {
		Company existingCompany = getCompanyById(cId);
		if (existingCompany != null) {
			companyRepository.save(company);
			return company;
		}
		return null;

	}

	public boolean deleteCompanyById(String cId) {
		Company company = getCompanyById(cId);
		if (company != null) {
			companyRepository.deleteById(cId);
			return true;
		}
		return false;
	}
}
