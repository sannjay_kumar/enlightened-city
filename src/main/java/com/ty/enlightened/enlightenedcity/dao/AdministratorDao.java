package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.Administrator;
import com.ty.enlightened.enlightenedcity.repository.AdministratorRepository;

@Repository
public class AdministratorDao {
	@Autowired
	private AdministratorRepository repository;
	
	public Administrator saveAdministrator(Administrator administrator) {
		return repository.save(administrator);
	}
	
	public Administrator getAdministratorById(String id) {
		Optional<Administrator> optional = repository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	
	public boolean deleteAdministratorById(String id) {
		Administrator administrator = getAdministratorById(id);
		if (administrator != null) {
			repository.delete(administrator);
			return true;
		}
		return false;
	}
	
	public List<Administrator> getAllAdministrators(){
		return repository.findAll();
	}
	
	public Administrator updateAdministrator(String id, Administrator administrator) {
		Administrator administrator2 = getAdministratorById(id);
		if (administrator2 != null) {
			return repository.save(administrator);
		}
		return null;
	}
}
