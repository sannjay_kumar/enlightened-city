package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.Booking;
import com.ty.enlightened.enlightenedcity.dto.Hotel;
import com.ty.enlightened.enlightenedcity.repository.BookingRepository;

@Repository
public class BookingDao {
	@Autowired
	private BookingRepository repo;
	@Autowired
	private HotelDao hdao;
	
	public Booking saveBooking(String hid, Booking booking) {
		Hotel hotel = hdao.hotelById(hid);
		if (hotel != null) {
			booking.setHotel(hotel);
			return repo.save(booking);
		}
		return null;
	}
	public Booking getBookingById(String bookid) {
		Optional<Booking> option = repo.findById(bookid);
		if (option.isEmpty()) {
			return null;
		}
		return option.get();
	}

	public List<Booking> getAllBooking(String hotelid) {
		Hotel hotel = hdao.hotelById(hotelid);
		if (hotel != null) {
			List<Booking> li = hotel.getBooking();
			return li;
		}
		return null;
	}

	public void deleteBooking(String bookid) {
		Booking booking = getBookingById(bookid);
		repo.delete(booking);
	}
	public Booking updateBooking(String bookid,Booking booking) {
		Booking existbook=getBookingById(bookid);
		if(existbook!=null) {
			repo.save(booking);
			return booking;
		}
		return null;
	}
}
