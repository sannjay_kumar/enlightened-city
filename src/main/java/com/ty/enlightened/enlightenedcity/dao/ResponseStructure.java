package com.ty.enlightened.enlightenedcity.dao;

import lombok.Data;

@Data
public class ResponseStructure<T> {
	private int status;
	private String message;
	private T data;
	
}
