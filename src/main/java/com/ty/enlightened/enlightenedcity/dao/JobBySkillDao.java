package com.ty.enlightened.enlightenedcity.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.JobBySkill;
import com.ty.enlightened.enlightenedcity.repository.JobBySkillRepository;

@Repository
public class JobBySkillDao {

	@Autowired
	private JobBySkillRepository jobBySkillRepository;

	public JobBySkill saveJobBySkill(JobBySkill jobBySkill) {
		return jobBySkillRepository.save(jobBySkill);
	}

	public JobBySkill getJobBySkillById(String id) {
		Optional<JobBySkill> optional = jobBySkillRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public JobBySkill updateJobBySkillById(JobBySkill jobBySkill, String id) {
		JobBySkill existingJobBySkill = getJobBySkillById(id);
		if (existingJobBySkill != null) {
			jobBySkillRepository.save(jobBySkill);
			return jobBySkill;
		}
		return null;

	}

	public boolean deleteJobBySkillById(String id) {
		JobBySkill jobBySkill = getJobBySkillById(id);
		if (jobBySkill != null) {
			jobBySkillRepository.deleteById(id);
			return true;
		}
		return false;
	}
}
