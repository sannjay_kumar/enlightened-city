package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.SportsClub;
import com.ty.enlightened.enlightenedcity.repository.SportClubRepository;

@Repository
public class SportsClubDao {
	@Autowired
	private SportClubRepository repository;
	
	public SportsClub saveSportsClub(SportsClub sportsClub) {
		return repository.save(sportsClub);
	}
	
	public SportsClub getSportsClubById(String id) {
		Optional<SportsClub> optional = repository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	
	public boolean deleteSportsClubById(String id) {
		SportsClub sportsClub = getSportsClubById(id);
		if (sportsClub != null) {
			repository.delete(sportsClub);
			return true;
		}
		return false;
	}
	
	public List<SportsClub> getAllSportsClubs(){
		return repository.findAll();
	}
	
	public SportsClub updateSportsClub(String id, SportsClub sportsClub) {
		SportsClub sportsClub2 = getSportsClubById(id);
		if (sportsClub2 != null) {
			return repository.save(sportsClub);
		}
		return null;
	}
}
