package com.ty.enlightened.enlightenedcity.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.JobLocation;
import com.ty.enlightened.enlightenedcity.repository.JobLocationRepository;

@Repository
public class JobLocationDao {

	@Autowired
	private JobLocationRepository jobLocationRepository;

	public JobLocation saveJobLocation(JobLocation jobByLocation) {
		return jobLocationRepository.save(jobByLocation);
	}

	public JobLocation getJobLocationById(String id) {
		Optional<JobLocation> optional = jobLocationRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public JobLocation updateJobLocationById(JobLocation jobByLocation, String id) {
		JobLocation existingJobLocation = getJobLocationById(id);
		if (existingJobLocation != null) {
			jobLocationRepository.save(jobByLocation);
			return jobByLocation;
		}
		return null;

	}

	public boolean deleteJobLocationById(String id) {
		JobLocation jobByLocation = getJobLocationById(id);
		if (jobByLocation != null) {
			jobLocationRepository.deleteById(id);
			return true;
		}
		return false;
	}
}
