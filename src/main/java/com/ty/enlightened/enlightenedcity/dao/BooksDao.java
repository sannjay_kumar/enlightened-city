package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.Books;
import com.ty.enlightened.enlightenedcity.repository.BooksRepository;

@Repository
public class BooksDao {
	@Autowired
	private BooksRepository repository;
	
	public Books saveBooks(Books books) {
		return repository.save(books);
	}
	
	public Books getBooksByName(String name) {
		return repository.findByName(name);
	}
	
	public Books getBooksById(String id) {
		Optional<Books> optional = repository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	
	public List<Books> getAllBooks(){
		return repository.findAll();
	}
	
	public boolean deleteBooksById(String id) {
		Books books = getBooksById(id);
		if (books != null) {
			repository.delete(books);
			return true;
		}
		return false;
	}
	
	public Books updateBooks(String id, Books books) {
		Books extbooks = getBooksById(id);
		if (extbooks != null) {
			return repository.save(books);
		}
		return null;
	}
}
