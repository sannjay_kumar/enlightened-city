package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.TouristPlaces;
import com.ty.enlightened.enlightenedcity.repository.TouristPlaceRepository;

@Repository
public class TouristPlaceDao {
	
	@Autowired
	private TouristPlaceRepository touristPlaceRepository;

	
	public TouristPlaces save(TouristPlaces places) {
	TouristPlaces touristPlaces=touristPlaceRepository.save(places);
	return touristPlaces;
	}
	
	public TouristPlaces touristPlacesById(String id) {
		Optional<TouristPlaces> opt=touristPlaceRepository.findById(id);
		if(opt.isEmpty()) {
			return null;
		}
		return opt.get();
	}
	
	public List<TouristPlaces> getAllTouristPlaces(){
		List<TouristPlaces> ls =touristPlaceRepository.findAll();
		return ls;
	}
	
	public void deleteById(String id) {
		TouristPlaces tourist=touristPlacesById(id);
		if(tourist!=null) {
			touristPlaceRepository.deleteById(id);
		}
	}
	
	public TouristPlaces updateTPlacesById(String id,TouristPlaces touristPlaces) {
		TouristPlaces tPlaces=touristPlacesById(id);
		if(tPlaces!=null) {
			return touristPlaceRepository.save(touristPlaces);
		}
		return null;
	}
}
