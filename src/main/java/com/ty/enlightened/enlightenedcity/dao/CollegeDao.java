package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.College;
import com.ty.enlightened.enlightenedcity.repository.CollegeRepository;

@Repository
public class CollegeDao {
	@Autowired
	private CollegeRepository repository;
	
	public College saveCollege(College college) {
		return repository.save(college);
	}
	
	public College getCollegeByName(String name) {
		return repository.findByName(name);
	}
	
	public College getCollegeById(String id) {
		Optional<College> optional = repository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	
	public List<College> getAllColleges(){
		return repository.findAll();
	}
	
	public boolean deleteCollegeById(String id) {
		College college = getCollegeById(id);
		if (college != null) {
			repository.delete(college);
			return true;
		}
		return false;
	}
	
	public College updateCollegeById(String id, College college) {
		College extcollege = getCollegeById(id);
		if (extcollege != null) {
			return repository.save(college);
		}
		return null;
	}
}
