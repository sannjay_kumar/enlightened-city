package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.Library;
import com.ty.enlightened.enlightenedcity.repository.LibraryRepository;

@Repository
public class LibraryDao {
	@Autowired
	private LibraryRepository repository;
	
	public Library saveLibrary(Library library) {
		return repository.save(library);
	}
	
	public Library getLibraryById(String id) {
		Optional<Library> optional = repository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	
	public List<Library> getAllLibraries(){
		return repository.findAll();
	}
	
	public boolean deleteLibraryById(String id) {
		Library library = getLibraryById(id);
		if (library != null) {
			repository.delete(library);
			return true;
		}
		return false;
	}
	
	public Library updateLibraryById(String id, Library library) {
		Library extlibrary = getLibraryById(id);
		if (extlibrary != null) {
			return repository.save(library);
		}
		return null;
	}
}
