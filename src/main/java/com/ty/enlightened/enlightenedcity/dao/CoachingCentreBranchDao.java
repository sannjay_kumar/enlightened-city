package com.ty.enlightened.enlightenedcity.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.CoachingCentreBranch;
import com.ty.enlightened.enlightenedcity.repository.CoachingCentreBranchRepository;
@Repository
public class CoachingCentreBranchDao {
	@Autowired
	private CoachingCentreBranchRepository centreRepository;

	public CoachingCentreBranch saveCoachingCentr(CoachingCentreBranch coachingCentreBranch) {
		return centreRepository.save(coachingCentreBranch);
	}

	public CoachingCentreBranch updateCoachingCentreBranch(CoachingCentreBranch coachingCentreBranch, String id) {
		CoachingCentreBranch existingCoachingCentreBranch = getCoachingCentreBranchById(id);
		if (existingCoachingCentreBranch != null) {
			centreRepository.save(coachingCentreBranch);
			return coachingCentreBranch;
		}
		return null;
	}

	public CoachingCentreBranch getCoachingCentreBranchById(String id) {
		Optional<CoachingCentreBranch> optional = centreRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}
	public boolean deleteCoachingCentreBranchById(String id) {
		CoachingCentreBranch coachingCentreBranch = getCoachingCentreBranchById(id);
		if (coachingCentreBranch != null) {
			centreRepository.delete(coachingCentreBranch);
			return true;
		}
		return false;

	}

	}



