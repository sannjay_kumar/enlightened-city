package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ty.enlightened.enlightenedcity.dto.Hotel;
import com.ty.enlightened.enlightenedcity.repository.HotelRepository;

@Repository
public class HotelDao {
	@Autowired
	private HotelRepository repo;

	public Hotel saveHotel(Hotel hotel) {

		// List<Booking> booking= bdao.getBookingById(bookid);
		// hotel.setBooking(booking);
		return repo.save(hotel);
	}

	public Hotel hotelById(String hotelid) {
		Optional<Hotel> option = repo.findById(hotelid);
		if (option.isEmpty()) {
			return null;
		}
		return option.get();
	}

	public List<Hotel> getAllHotel(String area) {
		List<Hotel> hotels = repo.findByArea(area);
		if(hotels!=null) {
		return hotels;
		}
		return null;
	}

	public void deleteHotel(String hotelid) {
		Hotel hotel = hotelById(hotelid);
		repo.delete(hotel);
	}
	public Hotel updateHotel(String hid,Hotel hotel) {
		Hotel existhotel=hotelById(hid);
		if(existhotel!=null) {
			repo.save(hotel);
			return hotel;
		}
		return null;
	}
}
