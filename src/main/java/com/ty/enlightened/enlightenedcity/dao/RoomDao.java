package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.Booking;
import com.ty.enlightened.enlightenedcity.dto.Room;
import com.ty.enlightened.enlightenedcity.repository.RoomRepository;

@Repository
public class RoomDao {
	@Autowired
	private RoomRepository roomRepository;
	@Autowired
	private BookingDao bookingDao;
	
	public Room saveRoom(String bid,Room room) {
		Booking booking= bookingDao.getBookingById(bid);
		if(booking!=null) {
			return roomRepository.save(room);
		}
		return null;
	}
	
	public Room getRoomById(String rid) {
		Room room=roomRepository.getById(rid);
		if(room!=null) {
			return room;
		}
		return null;
	}
	
	public List<Room> allRoom(){
		List<Room> ls=roomRepository.findAll();
		if(ls!=null) {
			return ls;
		}
		return null;
	}
	
	public boolean deleteRoomById(String rid) {
	   Room room=getRoomById(rid);
	   if(room!=null) {
		 roomRepository.delete(room);
		 return true;
	   }
	   return false;
	}
	
	public Room updateRoomById(String rid,Room room) {
		  Room room1=getRoomById(rid);
		  if(room1!=null) {
			 return saveRoom(rid, room);
		  }
		  return null;
	}
	
}
