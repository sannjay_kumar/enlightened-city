package com.ty.enlightened.enlightenedcity.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.JobByDesignation;
import com.ty.enlightened.enlightenedcity.repository.JobByDesignationRepository;

@Repository
public class JobByDesignationDao {

	@Autowired
	private JobByDesignationRepository jobByDesignationRepository;

	public JobByDesignation saveJobByDesignation(JobByDesignation jobByDesignation) {
		return jobByDesignationRepository.save(jobByDesignation);
	}

	public JobByDesignation getJobByDesignationById(String id) {
		Optional<JobByDesignation> optional = jobByDesignationRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public JobByDesignation updateJobByDesignationById(JobByDesignation jobByDesignation, String id) {
		JobByDesignation existingJobByDesignation = getJobByDesignationById(id);
		if (existingJobByDesignation != null) {
			jobByDesignationRepository.save(jobByDesignation);
			return jobByDesignation;
		}
		return null;

	}

	public boolean deleteJobByDesignationById(String id) {
		JobByDesignation jobByDesignation = getJobByDesignationById(id);
		if (jobByDesignation != null) {
			jobByDesignationRepository.deleteById(id);
			return true;
		}
		return false;
	}

}
