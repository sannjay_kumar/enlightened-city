package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.ShoppingMalls;
import com.ty.enlightened.enlightenedcity.repository.ShoppingMallRepository;

@Repository
public class ShoppingMallDao {
	@Autowired
	private ShoppingMallRepository shoppingMallRepository;
	
	public ShoppingMalls saveSMalls(ShoppingMalls shoppingMalls) {
		return shoppingMallRepository.save(shoppingMalls);
	}
	
	public ShoppingMalls getMallsById(String id) {
		Optional<ShoppingMalls> opt=shoppingMallRepository.findById(id);
		if(opt.isEmpty()) {
			return null;
		}
		return opt.get();
	}
	
	public List<ShoppingMalls> getAllMalls(){
		List<ShoppingMalls> ls=shoppingMallRepository.findAll();
		return ls;
	}
	
	public void deleteMallById(String id) {
		ShoppingMalls shoppingMalls=getMallsById(id);
		if(shoppingMalls!=null) {
			shoppingMallRepository.deleteById(id);
		}
	}
	
	public ShoppingMalls updateMallById(String id,ShoppingMalls malls) {
		ShoppingMalls shoppingMalls=getMallsById(id);
		if(shoppingMalls!=null) {
		 return shoppingMallRepository.save(malls);
		}
		return null;
			
	}
	

}
