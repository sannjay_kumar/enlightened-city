package com.ty.enlightened.enlightenedcity.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.enlightened.enlightenedcity.dto.SkillDevelopmentInstitute;
import com.ty.enlightened.enlightenedcity.repository.SkillDevelopmentInstituteRepository;
@Repository
public class SkillDevelopmentInstituteDao {
	@Autowired
	private SkillDevelopmentInstituteRepository centreRepository;

	public SkillDevelopmentInstitute saveSkillDevelopmentInstitute(SkillDevelopmentInstitute  skillDevelopmentInstitute) {
		return centreRepository.save(skillDevelopmentInstitute);
	}

	public SkillDevelopmentInstitute updateSkillDevelopmentInstitute(SkillDevelopmentInstitute skillDevelopmentInstitute, String id) {
		SkillDevelopmentInstitute existingSkillDevelopmentInstitute = getSkillDevelopmentInstituteById(id);
		if (existingSkillDevelopmentInstitute != null) {
			centreRepository.save(skillDevelopmentInstitute);
			return skillDevelopmentInstitute;
		}
		return null;
	}

	public SkillDevelopmentInstitute getSkillDevelopmentInstituteById(String id) {
		Optional<SkillDevelopmentInstitute> optional = centreRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public List<SkillDevelopmentInstitute> getAllSkillDevelopmentInstitute() {
		return centreRepository.findAll();
	}

	public boolean deleteSkillDevelopmentInstituteById(String id) {
		SkillDevelopmentInstitute skillDevelopmentInstitute = getSkillDevelopmentInstituteById(id);
		if (skillDevelopmentInstitute != null) {
			centreRepository.delete(skillDevelopmentInstitute);
			return true;
		}
		return false;

	}


}
